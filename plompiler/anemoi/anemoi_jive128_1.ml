(* compute one round and return intermediate values *)
let compute_one_round (type a) (module S : Ff_sig.PRIME with type t = a)
    (beta : a) (gamma : a) (delta : a) (g : a) (alpha_inv : a) (g2_p_1 : a)
    (x0 : a) (y0 : a) (kx : a) (ky : a) =
  (* -> Sbox *)
  (* w^5 = x0 - (beta y0^2 + gamma) *)
  let w_5 = S.(sub x0 ((beta * y0 * y0) + gamma)) in
  (* Computing w *)
  let w = S.(pow w_5 (to_z alpha_inv)) in
  (* v = y0 - w *)
  let v = S.sub y0 w in
  (* u = w^5 + beta * v^2 + delta *)
  let u = S.(w_5 + ((beta * v * v) + delta)) in
  (* -> Round Constant additon + Linear layer M, Figure 7.b from eprint Anemoi paper *)
  (* x1 = (u + kx) + g * (v + ky) *)
  let x1 = S.(u + kx + (g * (v + ky))) in
  (* y1 = (g * (u + kx) + (g^2 + 1) * (v + ky) *)
  let y1 = S.((g * (u + kx)) + (g2_p_1 * (v + ky))) in
  (w_5, w, v, u, x1, y1)
