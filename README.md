This repository contains aPlonK, the SNARK proving system that powers Epoxy, Tezos' validity rollup.

This repository is developed and maintained by the [cryptography team](https://research-development.nomadic-labs.com/files/cryptography.html) of Nomadic Labs.

# Structure of the repository

## aplonk

aPlonK is a PlonK-inspired proving system which focuses on proof-aggregation and distributed proof generation.

## plonk

PlonK is a modular implementation of the PlonK proving system which serves as a basis for APlonK.

## plompiler

Plompiler is a monadic Domain Specific Language embedded in OCaml that can be used to build circuits for PlonK or aPlonK.
It contains a library of gadgets including optimized implementation of the Anemoi and Poseidon hash functions as well as Schnorr signatures on JubJub.

## epoxy-tx

epoxy-tx is a transactional rollup for Epoxy which allows the transfer of Tezos' tickets.
The statements are written using plompiler.

## bls12-381-polynomial

bls12-381-polynomial contains C libraries and bindings to optimized implementations of various algorithms related to the bls12-381 curve, and polynomials based on it's scalar field. It is based on the blst library.

# Compiling

To setup correctly your local environment run the script
`./scripts/install_build_deps.sh` which will take care of creating a
local opam directory `_opam`.
