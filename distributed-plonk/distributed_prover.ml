(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Bls
open Utils
open Identities
module SMap = Plonk.SMap

module type S = sig
  module MP : Main_protocol.S
  module D : Distributed_wrapper.Enriched_process
  open MP

  val distributed_prover_main :
    workers:Distributed.Process_id.t list ->
    inputs_map:prover_inputs list SMap.t ->
    prover_public_parameters ->
    proof D.t
end

module Make (PC : Distribution.Kzg.PC_for_distribution_sig) = struct
  module PP = Distribution.Polynomial_protocol.Make (PC)
  module MP = Distribution.Main_protocol.Make (PP)
  module Msg = Message.Make (MP)
  module D = Distributed_wrapper.Make (Msg)

  let pc_distributed_prove_main ~workers pp transcript query_list answers_list
      secret_list prover_aux_list =
    let open MP.PP in
    let worker_message, state =
      PC.distributed_prove_main1 pp transcript query_list answers_list
        (secret_list @ [ SMap.empty ])
        (prover_aux_list @ [ PC.Commitment.empty_prover_aux ])
    in
    let open D in
    let request content ~index = Msg.PC_Distribution { index; content } in
    let reply (Msg.PC_Distribution_res { content; _ }) =
      Some (fun () -> return content)
    in
    let* prover_messages =
      dmap ~pids:workers ~request ~reply
        (List.init (List.length workers) (fun _ -> worker_message))
    in
    return (PC.distributed_prove_main2 state prover_messages)

  let pp_distributed_prove_main ~workers pc_public_parameters transcript ~n
      ~generator ~secrets_main ~eval_points_main ~eval_points_worker
      ~evaluated_perm_ids ~gx_gates ~nb_of_t_chunks =
    let open PP in
    let open D in
    let module M = Msg in
    let alpha, transcript = Fr_generation.random_fr transcript in
    let* ids_keys =
      dmap ~pids:workers
        ~request:(fun content ~index -> M.PP_prepare_ids { index; content })
        ~reply:(function
          | M.PP_prepare_ids_res { content; _ } ->
              Some (fun () -> return content))
        (List.map (Fun.const transcript) workers)
    in
    let perm_ids_keys, perm_ids =
      List.split @@ SMap.bindings evaluated_perm_ids
    in
    let all_ids_keys =
      List.concat (perm_ids_keys :: ids_keys) |> List.sort String.compare
    in
    let* batched_evaluated_ids_list =
      dmap ~pids:workers
        ~request:(fun content ~index -> M.PP_commit_to_t { index; content })
        ~reply:(function
          | M.PP_commit_to_t_res { content; _ } ->
              Some (fun () -> return content))
        (List.map (Fun.const (all_ids_keys, alpha)) workers)
    in
    let batched_perm =
      let powers_map =
        SMap.of_list @@ List.mapi (fun i s -> (s, i)) all_ids_keys
      in
      let powers =
        List.map (fun s -> SMap.find s powers_map) perm_ids_keys
        |> List.map (fun i -> Scalar.pow alpha @@ Z.of_int i)
      in
      Evaluations.linear_c ~evaluations:perm_ids ~linear_coeffs:powers ()
    in
    let batched_ids =
      Evaluations.linear_c
        ~evaluations:(batched_perm :: batched_evaluated_ids_list)
        ()
    in
    (* batched_ids contains just one element, so [alpha] will not be used in
       the call to compute_t; this is intended *)
    let t =
      compute_t ~n ~alpha ~nb_of_t_chunks (SMap.singleton "batched" batched_ids)
    in
    let cm_t, t_prover_aux = PC.Commitment.commit pc_public_parameters t in
    let transcript = Transcript.expand PC.Commitment.t cm_t transcript in
    let* pc_answers_worker =
      dmap ~pids:workers
        ~request:(fun content ~index -> M.PP_KZG_eval_at_x { index; content })
        ~reply:(function
          | M.PP_KZG_eval_at_x_res { content; _ } ->
              Some (fun () -> return content))
        (List.init (List.length workers) (Fun.const (transcript, gx_gates)))
    in
    let x, transcript = Fr_generation.random_fr transcript in
    let prover_aux_list_main = t_prover_aux :: List.map snd secrets_main in
    let polys_list_main = t :: List.map fst secrets_main in
    let eval_points_main = [ X ] :: eval_points_main in
    let query_list_main =
      List.map (convert_eval_points ~generator ~x) eval_points_main
    in
    let query_list_worker =
      List.map (convert_eval_points ~generator ~x) eval_points_worker
    in
    let pc_answers_main =
      List.map2 PC.evaluate polys_list_main query_list_main
    in
    let pc_answers =
      pc_answers_main @ Plonk.List.mapn PC.merge_answers pc_answers_worker
    in
    let* pc_proof, transcript =
      pc_distributed_prove_main ~workers pc_public_parameters transcript
        (query_list_main @ query_list_worker)
        pc_answers polys_list_main prover_aux_list_main
    in
    return ({ cm_t; pc_proof; pc_answers }, transcript)

  let distributed_prover_main ~(workers : Distributed.Process_id.t list)
      (common_pp, circuits_map) transcript ~inputs_map =
    let pp = (common_pp, SMap.sub_map inputs_map circuits_map) in
    let open D in
    let open MP in
    let module M = Msg in
    let nb_workers = List.length workers in
    (* send the public inputs to the workers *)
    let workers_inputs_map = split_inputs_map ~nb_workers inputs_map in

    let* replies0 =
      dmap ~pids:workers
        ~request:(fun content ~index -> M.Commit_to_wires { index; content })
        ~reply:(fun (M.Commit_to_wires_res { content; _ }) ->
          Some (fun () -> return content))
        workers_inputs_map
    in
    let commitment_wires = PP.PC.Commitment.recombine replies0 in

    (* update the transcript with the commitment to wires *)
    let transcript =
      Transcript.expand PP.PC.Commitment.t commitment_wires transcript
    in
    let* replies =
      dmap ~pids:workers
        ~request:(fun content ~index -> M.Commit_to_plook { index; content })
        ~reply:(fun (M.Commit_to_plook_res { content; _ }) ->
          Some (fun () -> return content))
        (List.init nb_workers (Fun.const transcript))
    in
    let* _ = lift_io @@ Lwt_io.flush_all () in

    let randomness, transcript = build_gates_randomness transcript in
    let recombine_batched_witness pieces =
      (* we want the last worker to be first to apply Horner's method *)
      let pieces = List.rev pieces in
      List.fold_left
        (fun acc m ->
          SMap.union
            (fun circuit_name witness_acc witness_m ->
              let n = List.length (SMap.find circuit_name inputs_map) in
              let chunk_size = Z.(cdiv (of_int n) (of_int nb_workers)) in
              let delta_factor = Scalar.pow randomness.delta chunk_size in
              let sum =
                SMap.mapi
                  (fun i w_acc ->
                    let w = SMap.find i witness_m in
                    Evaluations.(add w (mul_by_scalar delta_factor w_acc)))
                  witness_acc
              in
              Some sum)
            acc m)
        (List.hd pieces) (List.tl pieces)
    in
    (* TODO: move all this to a new function [shared_permutation_argument] *)
    let batched_wires_map =
      recombine_batched_witness
        (List.map (fun r -> r.batched_wires_map) replies)
    in

    let f_map_perm = build_f_map_perm pp randomness batched_wires_map in

    let evaluated_perm_ids =
      let evaluations =
        let batched_wires_polys =
          build_batched_witness_polys common_pp batched_wires_map
        in
        build_evaluations pp
          (SMap.union_disjoint f_map_perm batched_wires_polys)
      in
      (build_perm_identities pp (randomness.beta_perm, randomness.gamma_perm))
        evaluations
    in

    let cmt_perm, perm_prover_aux =
      let srs = common_pp.pp_public_parameters in
      PP.PC.Commitment.commit srs f_map_perm
    in
    let cmt_plook =
      PP.PC.Commitment.recombine (List.map (fun r -> r.cmt_plookup) replies)
    in
    let cmt_perm_and_plook =
      PP.PC.Commitment.recombine [ cmt_plook; cmt_perm ]
    in
    (* update the transcript with the commitment to perm and plook *)
    let transcript =
      Transcript.expand PP.PC.Commitment.t cmt_perm_and_plook transcript
    in
    let f_map_plook =
      SMap.union_disjoint_list (List.map (fun r -> r.f_map_plook) replies)
    in
    let f_perm_and_plook_map = SMap.union_disjoint f_map_perm f_map_plook in
    let perm_and_plook_prover_aux =
      PP.PC.Commitment.recombine_prover_aux
        (perm_prover_aux :: List.map (fun r -> r.plook_prover_aux) replies)
    in
    let secrets_main =
      [
        (common_pp.g_map, common_pp.g_prover_aux);
        (f_perm_and_plook_map, perm_and_plook_prover_aux);
      ]
    in
    let gx_gates =
      SMap.exists
        (fun _ c -> Gates.exists_gx_composition ~gates:c.gates)
        circuits_map
    in
    let eval_points =
      let ultra = SMap.exists (fun _ c -> c.ultra) circuits_map in
      eval_points ultra gx_gates 0
    in
    let eval_points_main, eval_points_worker =
      Plonk.List.split_n 2 eval_points
    in
    let generator = Domain.get common_pp.domain 1 in
    let* pp_proof, transcript =
      pp_distributed_prove_main ~workers common_pp.pp_public_parameters
        transcript ~n:common_pp.n ~generator ~secrets_main ~eval_points_main
        ~eval_points_worker ~evaluated_perm_ids ~gx_gates
        ~nb_of_t_chunks:common_pp.nb_of_t_chunks
    in
    return
      ( pp_proof,
        transcript,
        ( cmt_perm_and_plook,
          commitment_wires,
          randomness.beta_perm,
          randomness.gamma_perm,
          randomness.delta ) )

  let distributed_prover_main ~workers ~inputs_map
      (prover_pp : MP.prover_public_parameters) =
    let open D in
    let open MP in
    if prover_pp.common_pp.zk then
      failwith "Distribution with ZK is not supported";
    (* add the PI in the transcript *)
    let transcript =
      hash_verifier_inputs prover_pp.transcript
      @@ to_verifier_inputs prover_pp inputs_map
    in
    let* pp_proof, _transcript, (perm_and_plook, wires_cm, _, _, _) =
      distributed_prover_main ~workers
        (prover_pp.common_pp, prover_pp.circuits_map)
        transcript ~inputs_map
    in
    return { perm_and_plook; wires_cm; pp_proof }
end

module PC = Distribution.Kzg_pack.Kzg_pack_impl
module PP = Distribution.Polynomial_protocol.Make (PC)
