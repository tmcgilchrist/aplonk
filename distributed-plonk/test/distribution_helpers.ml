(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Scalar = Bls12_381.Fr
(* open Helpers *)

module Port : sig
  val make : unit -> int
end = struct
  let port = ref 50000

  let make () =
    let p = !port in
    port := p + 1;
    p
end

let zero = Scalar.zero
let one = Scalar.one
let two = Scalar.add one one
let mone = Scalar.negate one

(* build a Fibonacci circuit of 2^i constraints *)
let fibonacci_circuit i =
  let n = Int.shift_left 1 i in
  let m = n + 2 in
  let circuit =
    let l = 0 in
    let wires =
      Plonk.Circuit.make_wires
        ~a:(List.init n (fun i -> i))
        ~b:(List.init n (fun i -> i + 1))
        ~c:(List.init n (fun i -> i + 2))
        ()
    in
    let gates =
      Plonk.Circuit.make_gates
        ~qo:(List.init n (fun _ -> mone))
        ~qm:(List.init n (fun _ -> one))
        ~ql:(List.init n (fun _ -> one))
        ~qr:(List.init n (fun _ -> one))
        ()
    in
    Plonk.Circuit.make ~wires ~gates ~public_input_size:l ()
  in
  let x = Array.init m (fun _ -> Scalar.one) in
  for i = 2 to m - 1 do
    x.(i) <-
      Scalar.add
        (Scalar.add x.(i - 1) x.(i - 2))
        (Scalar.mul x.(i - 1) x.(i - 2))
  done;
  (circuit, x)
