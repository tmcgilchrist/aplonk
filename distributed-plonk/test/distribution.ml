(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Plonk_test.Helpers
open Distributed_plonk_test.Distribution_helpers
open Distributed_plonk
module DP = Distributed_prover.Make (Distributed_prover.PC)
module Runner = Master_runner.Make (DP.D)
module MP = DP.MP

let run_master ~self_node ~nodes =
  Printf.printf "Setup\n";
  let nb_rep = 12 in
  let fib_circuit, fib_x = fibonacci_circuit 14 in

  let circuit_map =
    Plonk.SMap.of_list
      [ ("cfib", (fib_circuit, nb_rep)); ("cfib2", (fib_circuit, nb_rep)) ]
  in
  let pp_prover, pp_verifier =
    MP.setup ~zero_knowledge:false circuit_map ~srs
  in
  let oc = open_out "pp_prover" in
  let b = Plompiler.Utils.to_bytes DP.MP.prover_public_parameters_t pp_prover in
  output_bytes oc b;
  close_out oc;
  let x_map =
    Plonk.SMap.of_list [ ("cfib", List.init nb_rep (Fun.const fib_x)) ]
  in
  let inputs =
    Plonk.SMap.map
      (List.map (fun witness -> MP.{ witness; input_commitments = [] }))
      x_map
  in
  let verifier_inputs = MP.to_verifier_inputs pp_prover inputs in

  let t1 = Unix.gettimeofday () in

  let proof =
    Runner.run ~self_node ~nodes
      DP.(distributed_prover_main ~inputs_map:inputs pp_prover)
  in
  let t2 = Unix.gettimeofday () in
  Printf.printf "Prover time: %4.2f\n" (t2 -. t1);
  let b =
    try MP.verify pp_verifier ~inputs:verifier_inputs proof with _e -> false
  in
  Printf.printf "verified: %b\n" b

let parse_nodes s =
  (* Expected as ip:port;ip:port... *)
  let node_strings = String.split_on_char ',' s in
  List.mapi
    (fun i ns ->
      let n = String.split_on_char ':' ns in
      Runner.
        {
          ip = List.hd n;
          port = int_of_string @@ List.nth n 1;
          name = "worker" ^ string_of_int i;
        })
    node_strings

let () =
  let args = Sys.argv in
  let ip = args.(1) in
  let port = int_of_string args.(2) in
  let nodes = parse_nodes args.(3) in
  run_master ~self_node:Runner.{ name = "master"; ip; port } ~nodes
