(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Plonk_test.Helpers
open Distributed_plonk_test.Distribution_helpers
open Distributed_plonk

let test_distribution pc () =
  let module PC = (val pc : Distribution.Kzg.PC_for_distribution_sig) in
  let module DP = Distributed_prover.Make (PC) in
  let module Worker = Worker.Make (PC) in
  let module Worker0 = Distributed_wrapper.Make (Worker.Msg) in
  let module Worker1 = Distributed_wrapper.Make (Worker.Msg) in
  let module Master = DP.D in
  let loopback = "127.0.0.1" in
  let master_port = Port.make () in
  let worker0_port = Port.make () in
  let worker1_port = Port.make () in
  let worker0_config =
    let open Worker0 in
    Remote
      {
        Remote_config.node_name = "worker0";
        Remote_config.local_port = worker0_port;
        Remote_config.connection_backlog = 10;
        Remote_config.node_ip = loopback;
        Remote_config.remote_nodes = [];
      }
  in
  let worker1_config =
    let open Worker1 in
    Remote
      {
        Remote_config.node_name = "worker1";
        Remote_config.local_port = worker1_port;
        Remote_config.connection_backlog = 10;
        Remote_config.node_ip = loopback;
        Remote_config.remote_nodes = [];
      }
  in
  let master_config =
    let open Master in
    Remote
      {
        Remote_config.node_name = "master";
        Remote_config.local_port = master_port;
        Remote_config.connection_backlog = 10;
        Remote_config.node_ip = loopback;
        Remote_config.remote_nodes =
          [
            (loopback, worker0_port, "worker0");
            (loopback, worker1_port, "worker1");
          ];
      }
  in
  let nb_rep = 12 in
  let fib_circuit, fib_x = fibonacci_circuit 8 in
  let circuit_map =
    Plonk.SMap.of_list
      [ ("cfib", (fib_circuit, nb_rep)); ("cfib2", (fib_circuit, nb_rep)) ]
  in
  let pp_prover, pp_verifier =
    DP.MP.setup ~zero_knowledge:false circuit_map ~srs
  in
  let oc = open_out "pp_prover" in
  let b = Plompiler.Utils.to_bytes DP.MP.prover_public_parameters_t pp_prover in
  output_bytes oc b;
  close_out oc;
  let x_map =
    Plonk.SMap.of_list [ ("cfib", List.init nb_rep (Fun.const fib_x)) ]
  in
  let inputs =
    Plonk.SMap.map
      (List.map (fun witness -> DP.MP.{ witness; input_commitments = [] }))
      x_map
  in
  let verifier_inputs = DP.MP.to_verifier_inputs pp_prover inputs in
  let master_proc m ~ret () =
    let open Master in
    let* nodes = get_remote_nodes in
    let* pid_to_send_to = get_self_pid in
    (* Dummy worker process in the master *)
    let* () = register "worker" (fun _pid () -> return ()) in
    (* spawn and monitor a process on the remote node atomically *)
    let* remote_pids =
      mapM
        (fun n ->
          let+ pid, _ref =
            spawn ~monitor:true n (Registered "worker") pid_to_send_to
          in
          pid)
        nodes
    in
    let+ r = m ~workers:remote_pids in
    ret := Some r;
    ()
  in
  let ret = ref None in
  Lwt.async (fun () ->
      Worker0.run_node
        ~process:(fun () -> Worker0.register "worker" Worker.worker_proc)
        worker0_config);
  Lwt.async (fun () ->
      Worker1.run_node
        ~process:(fun () -> Worker1.register "worker" Worker.worker_proc)
        worker1_config);
  Lwt_main.run
    (let open Master_runner.Make (Master) in
    Master.run_node
      ~process:
        (master_proc
           DP.(distributed_prover_main ~inputs_map:inputs pp_prover)
           ~ret)
      master_config);
  let proof = Option.get !ret in
  let b =
    try DP.MP.verify pp_verifier ~inputs:verifier_inputs proof
    with _e -> false
  in
  assert b

let kzg =
  (module Distribution.Kzg.Kzg_impl : Distribution.Kzg.PC_for_distribution_sig)

let kzg_pack =
  (module Distribution.Kzg_pack.Kzg_pack_impl
  : Distribution.Kzg.PC_for_distribution_sig)

let tests =
  List.map
    (fun (name, f) -> Alcotest.test_case name `Quick f)
    [
      ("test_distribution_kzg", test_distribution kzg);
      ("test_distribution_kzg_pack", test_distribution kzg_pack);
    ]
