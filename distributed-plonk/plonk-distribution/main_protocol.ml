(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Plonk.Bls
open Plonk.Utils
open Plonk.Identities
module SMap = Plonk.SMap

module type S = sig
  module PP : Polynomial_protocol.S

  type common_prover_pp [@@deriving repr]
  type circuit_prover_pp [@@deriving repr]

  type prover_public_parameters = {
    common_pp : common_prover_pp;
    circuits_map : circuit_prover_pp SMap.t;
    transcript : PP.transcript;
  }
  [@@deriving repr]

  include
    Plonk.Main_protocol.S
      with type prover_public_parameters := prover_public_parameters

  type worker_inputs [@@deriving repr]

  val split_inputs_map :
    nb_workers:int ->
    circuit_prover_input list SMap.t ->
    worker_inputs SMap.t list

  type commit_to_wires_reply = PP.PC.Commitment.t [@@deriving repr]

  (* shifts_maps binds circuits names to pairs of integers.
     'c1' -> (7, 20) means that 20 proofs are expected for circuit 'c1' and
     there must be a shift of 7 in indexing considering the worker is starting
     at proof No. 7 *)
  type commit_to_wires_remember = {
    all_f_wires : Poly.t SMap.t;
    wires_list_map : scalar array SMap.t list SMap.t;
    inputs_map : circuit_prover_input list SMap.t;
    shifts_map : (int * int) SMap.t;
    srs : PP.prover_public_parameters;
    f_wires : Poly.t SMap.t list SMap.t;
    cm_aux_wires : PP.PC.Commitment.prover_aux;
  }

  val worker_commit_to_wires :
    common_prover_pp * circuit_prover_pp SMap.t ->
    worker_inputs SMap.t ->
    commit_to_wires_reply * commit_to_wires_remember

  type commit_to_plook_reply = {
    batched_wires_map : Evaluations.t SMap.t SMap.t;
    cmt_plookup : PP.PC.Commitment.t;
    f_map_plook : Poly.t SMap.t;
    plook_prover_aux : PP.PC.Commitment.prover_aux;
  }
  [@@deriving repr]

  type commit_to_plook_remember = {
    beta_plookup : scalar;
    gamma_plookup : scalar;
  }

  val commit_to_plook :
    common_prover_pp * circuit_prover_pp SMap.t ->
    (int * int) SMap.t ->
    PP.prover_public_parameters ->
    bytes ->
    scalar array SMap.t list SMap.t ->
    commit_to_plook_reply * commit_to_plook_remember

  val batch_evaluated_ids :
    alpha:scalar -> Evaluations.t SMap.t -> string list -> Evaluations.t

  val kzg_eval_at_x :
    PP.transcript ->
    (PP.PC.secret * PP.PC.Commitment.prover_aux) list ->
    scalar ->
    bool ->
    PP.PC.answer list
end

(* [build_all_keys strs shifts_map] returns a list of prefixed [strs],
   deduced from the [shifts_map] (that contains circuits names binded with,
   among others, the number of proofs) that corresponds to all the
   names of the [strs] polynomials that will be committed for the proof
*)
let build_all_keys names shifts_map =
  let build_all_names prefix n name =
    List.init n (fun i -> SMap.Aggregation.add_prefix ~n ~i prefix name)
  in
  SMap.mapi
    (fun prefix (_i, n) -> List.concat_map (build_all_names prefix n) names)
    shifts_map
  |> SMap.values |> List.concat

module Make (PP : Polynomial_protocol.S) = struct
  module MP = Plonk.Main_protocol.Make_impl (PP)
  include (MP : module type of MP with module PP := PP)
  module PP = PP
  module Commitment = Commitment

  type commit_to_wires_reply = Commitment.t [@@deriving repr]

  type worker_inputs = { inputs : circuit_prover_input list; shift : int * int }
  [@@deriving repr]

  include Prover

  let split_inputs_map ~nb_workers inputs_map =
    let list_range i1 i2 = List.filteri (fun i _ -> i1 <= i && i < i2) in
    List.map
      (fun i ->
        SMap.map
          (fun l ->
            let n = List.length l in
            let chunk_size =
              Z.(cdiv (of_int n) (of_int nb_workers) |> to_int)
            in
            let inputs = list_range (chunk_size * i) (chunk_size * (i + 1)) l in
            let shift = (chunk_size * i, n) in
            { inputs; shift })
          inputs_map)
      (List.init nb_workers Fun.id)

  type commit_to_plook_reply = {
    batched_wires_map : Evaluations.t SMap.t SMap.t;
    cmt_plookup : Commitment.t;
    f_map_plook : Poly.t SMap.t;
    plook_prover_aux : Commitment.prover_aux;
  }
  [@@deriving repr]

  type commit_to_plook_remember = {
    beta_plookup : scalar;
    gamma_plookup : scalar;
  }

  type commit_to_wires_remember = {
    all_f_wires : Poly.t SMap.t;
    wires_list_map : scalar array SMap.t list SMap.t;
    inputs_map : circuit_prover_input list SMap.t;
    shifts_map : (int * int) SMap.t;
    srs : PP.prover_public_parameters;
    f_wires : Poly.t SMap.t list SMap.t;
    cm_aux_wires : PP.PC.Commitment.prover_aux;
  }

  let worker_commit_to_wires (common_pp, circuits_map) worker_inputs_map =
    let inputs_map = SMap.map (fun wi -> wi.inputs) worker_inputs_map in
    let shifts_map = SMap.map (fun wi -> wi.shift) worker_inputs_map in
    let srs = common_pp.pp_public_parameters in
    let all_keys = build_all_keys [ "a"; "b"; "c" ] shifts_map in
    let wires_list_map, f_wires, _, all_f_wires, cm_wires, cm_aux_wires =
      commit_to_wires ~all_keys ~shifts_map (common_pp, circuits_map) inputs_map
    in
    ( cm_wires,
      {
        all_f_wires;
        wires_list_map;
        inputs_map;
        shifts_map;
        srs;
        f_wires;
        cm_aux_wires;
      } )

  let commit_to_plook pp shifts_map srs transcript f_wires_list_map =
    let rd, _transcript = build_gates_randomness transcript in
    let batched_wires_map = build_batched_wires_values rd f_wires_list_map in
    (* ******************************************* *)
    let f_map_plook = build_f_map_plook ~shifts_map pp rd f_wires_list_map in
    (* commit to the plookup polynomials *)
    let cmt_plookup, plook_prover_aux =
      (* FIXME: implement Plookup *)
      let all_keys = build_all_keys [ "plook" ] shifts_map in
      PP.PC.Commitment.commit ~all_keys srs f_map_plook
    in
    ( { batched_wires_map; cmt_plookup; f_map_plook; plook_prover_aux },
      { beta_plookup = rd.beta_plook; gamma_plookup = rd.gamma_plook } )

  let batch_evaluated_ids ~alpha evaluated_ids all_ids_keys =
    let powers_map =
      SMap.of_list @@ List.mapi (fun i s -> (s, i)) all_ids_keys
    in
    let ids_keys, evaluations = List.split @@ SMap.bindings evaluated_ids in
    let powers =
      List.map (fun s -> SMap.find s powers_map) ids_keys
      |> List.map (fun i -> Scalar.pow alpha @@ Z.of_int i)
    in
    Evaluations.linear_c ~evaluations ~linear_coeffs:powers ()

  let kzg_eval_at_x transcript secrets_worker generator gx_gates =
    let eval_points_worker =
      [ List.hd @@ List.rev @@ eval_points false gx_gates 0 ]
    in
    let x, _transcript = Fr_generation.random_fr transcript in
    let polys_list_worker = List.map fst secrets_worker in
    let query_list_worker =
      List.map (convert_eval_points ~generator ~x) eval_points_worker
    in
    List.map2 PP.PC.evaluate polys_list_worker query_list_worker

  (* Same as Plonk.Main_protocol.build_batched_witness_poly, but the IFFT
     version every times.
     Because I don’t know how to use f_wires in distributed_prover
  *)
  let build_batched_witness_polys common_pp batched_witnesses =
    let batched_witness_polys =
      SMap.map
        (fun batched_witness ->
          (* we apply an IFFT on the batched witness *)
          batched_wires_poly_of_batched_wires common_pp batched_witness
            (Scalar.zero, []))
        batched_witnesses
    in
    batched_witness_polys |> SMap.Aggregation.smap_of_smap_smap
end
