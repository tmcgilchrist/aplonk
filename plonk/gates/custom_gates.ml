(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Bls
open Identities
module L = Plompiler.LibCircuit

module Aggregator = struct
  type public = { public_inputs : Scalar.t array; input_coms_size : int }

  module Gates = struct
    let one = Scalar.one
    let mone = Scalar.negate one
    let left = "a"
    let right = "b"
    let output = "c"
    let com_label = "com"

    (* Block names to merge identities within, if identities are independent, use q_label instead.
       For instance, we want to have want AddLeft and Addright identities to be merged inside the Arithmetic block,
       we thus use "arith" as Map key for these gates identities.
       We also want to use the ECC point addition identity independently, as such we put ECCAdd gate's q_label as key. *)
    let arith = "Arith"

    let map_singleton =
      let map f t =
        let open L in
        let* x = t in
        ret (f x)
      in
      map (fun x -> [ x ])

    module type Base_sig = sig
      val q_label : string

      (* array.(i) = 1 <=> f is evaluated at (g^i)X *)
      val blinds : int array SMap.t
      val identity : string * int
      val gx_composition : bool

      val equations :
        q:Scalar.t ->
        a:Scalar.t ->
        b:Scalar.t ->
        c:Scalar.t ->
        ag:Scalar.t ->
        bg:Scalar.t ->
        cg:Scalar.t ->
        unit ->
        Scalar.t list

      val prover_identities :
        prefix_common:(string -> string) ->
        prefix:(string -> string) ->
        public:public ->
        domain:Domain.t ->
        prover_identities

      val verifier_identities :
        prefix_common:(string -> string) ->
        prefix:(string -> string) ->
        public:public ->
        generator:Scalar.t ->
        size_domain:int ->
        verifier_identities

      (* Give the size of the domain on which the identities
         of the gate need to have the evaluation of each of his polynomial
         divided by the size of the citcuit. *)
      val polynomials_degree : int SMap.t

      val cs :
        q:L.scalar L.repr ->
        a:L.scalar L.repr ->
        b:L.scalar L.repr ->
        c:L.scalar L.repr ->
        ag:L.scalar L.repr ->
        bg:L.scalar L.repr ->
        cg:L.scalar L.repr ->
        L.scalar L.repr list L.t
    end

    module type Params = sig
      val wire : string
      val selector : string
      val is_next : bool

      val cs :
        q:L.scalar L.repr ->
        a:L.scalar L.repr ->
        b:L.scalar L.repr ->
        c:L.scalar L.repr ->
        ag:L.scalar L.repr ->
        bg:L.scalar L.repr ->
        cg:L.scalar L.repr ->
        L.scalar L.repr list L.t
    end

    (* General functor to create Artih monomial gate that add wire *)
    module AddWire (Params : Params) : Base_sig = struct
      let q_label = Params.selector
      let identity = (arith, 1)
      let gx_composition = Params.is_next

      let equations ~q ~a ~b ~c ~ag ~bg ~cg () =
        let var =
          match Params.wire with
          | s when s = left -> if Params.is_next then ag else a
          | s when s = right -> if Params.is_next then bg else b
          | s when s = output -> if Params.is_next then cg else c
          | _ -> assert false
        in
        Scalar.[ q * var ]

      let blinds =
        let array = if Params.is_next then [| 0; 1 |] else [| 1; 0 |] in
        SMap.singleton Params.wire array

      let prover_identities ~prefix_common ~prefix ~public:_ ~domain :
          prover_identities =
       fun evaluations ->
        let poly_names = [ prefix_common q_label; prefix Params.wire ] in
        let composition_gx =
          if Params.is_next then ([ 0; 1 ], Domain.length domain)
          else ([ 0; 0 ], 1)
        in
        let res = Evaluations.find_evaluation evaluations "tmp_eval" in
        let res =
          Evaluations.mul ~res ~evaluations ~poly_names ~composition_gx ()
        in
        SMap.singleton (prefix @@ arith ^ ".0") res

      let verifier_identities ~prefix_common ~prefix ~public:_ ~generator:_
          ~size_domain:_ : verifier_identities =
       fun _ answers ->
        let q = get_answer answers X @@ prefix_common q_label in
        let w =
          let p = if Params.is_next then GX else X in
          get_answer answers p @@ prefix Params.wire
        in
        let res = Scalar.mul q w in
        SMap.singleton (prefix @@ arith ^ ".0") res

      let polynomials_degree = SMap.of_list [ (Params.wire, 2); (q_label, 2) ]
      let cs = Params.cs
    end

    (* Add next output gate
       Arith monomial
       degree : 2n
       advice selectors : None
       equations : + q×c
    *)
    module AddOutput = AddWire (struct
      let wire = output
      let selector = "qo"
      let is_next = false

      let cs ~q:qo ~a:_ ~b:_ ~c ~ag:_ ~bg:_ ~cg:_ =
        map_singleton (L.Num.mul qo c)
    end)

    (* Add next left gate
       Arith monomial
       degree : 2n
       advice selectors : None
       equations : + q×a
    *)
    module AddLeft = AddWire (struct
      let wire = left
      let selector = "ql"
      let is_next = false

      let cs ~q:ql ~a ~b:_ ~c:_ ~ag:_ ~bg:_ ~cg:_ =
        map_singleton (L.Num.mul ql a)
    end)

    (* Add next right gate
       Arith monomial
       degree : 2n
       advice selectors : None
       equations : + q×b
    *)
    module AddRight = AddWire (struct
      let wire = right
      let selector = "qr"
      let is_next = false

      let cs ~q:qr ~a:_ ~b ~c:_ ~ag:_ ~bg:_ ~cg:_ =
        map_singleton (L.Num.mul qr b)
    end)

    (* Add next output gate
       Arith monomial
       degree : 2n
       advice selectors : None
       equations : + q×cg
    *)

    module AddNextOutput = AddWire (struct
      let wire = output
      let selector = "qog"
      let is_next = true

      let cs ~q:qog ~a:_ ~b:_ ~c:_ ~ag:_ ~bg:_ ~cg =
        map_singleton (L.Num.mul qog cg)
    end)

    (* Add next left gate
       Arith monomial
       degree : 2n
       advice selectors : None
       equations : + q×ag
    *)
    module AddNextLeft = AddWire (struct
      let wire = left
      let selector = "qlg"
      let is_next = true

      let cs ~q:qlg ~a:_ ~b:_ ~c:_ ~ag ~bg:_ ~cg:_ =
        map_singleton (L.Num.mul qlg ag)
    end)

    (* Add next right gate
       Arith monomial
       degree : 2n
       advice selectors : None
       equations : + q×bg
    *)
    module AddNextRight = AddWire (struct
      let wire = right
      let selector = "qrg"
      let is_next = true

      let cs ~q:qrg ~a:_ ~b:_ ~c:_ ~ag:_ ~bg ~cg:_ =
        map_singleton (L.Num.mul qrg bg)
    end)

    (* Add constant
       Arith monomial
       degree : n
       advice selectors : None
       equations : + q
    *)
    module Constant : Base_sig = struct
      let q_label = "qc"
      let identity = (arith, 1)
      let gx_composition = false
      let equations ~q ~a:_ ~b:_ ~c:_ ~ag:_ ~bg:_ ~cg:_ () = [ q ]
      let blinds = SMap.empty

      let prover_identities ~prefix_common ~prefix ~public:_ ~domain:_ :
          prover_identities =
       fun evaluations ->
        let res = Evaluations.find_evaluation evaluations "tmp_eval" in
        (* This is copied because in sum_prover_queries it could
           be overwritten by the inplace addition. *)
        let res =
          Evaluations.copy ~res (SMap.find (prefix_common q_label) evaluations)
        in
        SMap.singleton (prefix @@ arith ^ ".0") res

      let verifier_identities ~prefix_common ~prefix ~public:_ ~generator:_
          ~size_domain:_ : verifier_identities =
       fun _ answers ->
        let res = get_answer answers X @@ prefix_common q_label in
        SMap.singleton (prefix @@ arith ^ ".0") res

      let polynomials_degree = SMap.empty
      let cs ~q:qc ~a:_ ~b:_ ~c:_ ~ag:_ ~bg:_ ~cg:_ = L.ret [ qc ]
    end

    (* Add multiplication
       Arith monomial
       degree : 3n
       advice selectors : None
       equations : + q×a×b
    *)
    module Multiplication : Base_sig = struct
      let q_label = "qm"
      let identity = (arith, 1)
      let gx_composition = false
      let equations ~q ~a ~b ~c:_ ~ag:_ ~bg:_ ~cg:_ () = Scalar.[ q * a * b ]
      let blinds = SMap.of_list [ (right, [| 1; 0 |]); (left, [| 1; 0 |]) ]

      let prover_identities ~prefix_common ~prefix ~public:_ ~domain:_ :
          prover_identities =
       fun evaluations ->
        let res = Evaluations.find_evaluation evaluations "tmp_eval" in
        let poly_names = [ prefix_common q_label; prefix left; prefix right ] in
        let res = Evaluations.mul ~res ~evaluations ~poly_names () in
        SMap.singleton (prefix @@ arith ^ ".0") res

      let verifier_identities ~prefix_common ~prefix ~public:_ ~generator:_
          ~size_domain:_ : verifier_identities =
       fun _ answers ->
        let q = get_answer answers X @@ prefix_common q_label in
        let l = get_answer answers X @@ prefix left in
        let r = get_answer answers X @@ prefix right in
        let res = Scalar.(q * l * r) in
        SMap.singleton (prefix @@ arith ^ ".0") res

      let polynomials_degree =
        SMap.of_list [ (left, 3); (right, 3); (q_label, 3) ]

      let cs ~q:qm ~a ~b ~c:_ ~ag:_ ~bg:_ ~cg:_ =
        let open L in
        map_singleton
          (let* tmp = Num.mul qm a in
           Num.mul tmp b)
    end

    (* Add left⁵
       Arith monomial
       degree : 6n
       advice selectors : None
       equations : + q×a⁵
    *)
    module X5A : Base_sig = struct
      let q_label = "qx5a"
      let identity = (arith, 1)
      let gx_composition = false

      let equations ~q ~a ~b:_ ~c:_ ~ag:_ ~bg:_ ~cg:_ () =
        Scalar.[ q * pow a (Z.of_int 5) ]

      let blinds = SMap.singleton left [| 1; 0 |]

      let prover_identities ~prefix_common ~prefix ~public:_ ~domain:_ :
          prover_identities =
       fun evaluations ->
        let q_label = prefix_common q_label in
        let left = prefix left in
        let res = Evaluations.find_evaluation evaluations "tmp_eval" in
        let res =
          Evaluations.mul ~res ~evaluations ~poly_names:[ q_label; left ]
            ~powers:[ 1; 5 ] ()
        in
        SMap.singleton (prefix @@ arith ^ ".0") res

      let verifier_identities ~prefix_common ~prefix ~public:_ ~generator:_
          ~size_domain:_ : verifier_identities =
       fun _ answers ->
        let q = get_answer answers X @@ prefix_common q_label in
        let l = get_answer answers X @@ prefix left in
        let l2 = Scalar.mul l l in
        let l4 = Scalar.mul l2 l2 in
        let l5 = Scalar.mul l4 l in
        let res = Scalar.mul q l5 in
        SMap.singleton (prefix @@ arith ^ ".0") res

      let polynomials_degree = SMap.of_list [ (left, 6); (q_label, 6) ]

      let cs ~q:qx5 ~a ~b:_ ~c:_ ~ag:_ ~bg:_ ~cg:_ =
        map_singleton
          L.(
            let* a5 = Num.pow5 a in
            Num.mul qx5 a5)
    end

    (* Add output⁵
       Arith monomial
       degree : 6n
       advice selectors : None
       equations : + q×c⁵
    *)
    module X5C : Base_sig = struct
      let q_label = "qx5c"
      let identity = (arith, 1)
      let gx_composition = false

      let equations ~q ~a:_ ~b:_ ~c ~ag:_ ~bg:_ ~cg:_ () =
        Scalar.[ q * pow c (Z.of_int 5) ]

      let blinds = SMap.singleton output [| 1; 0 |]

      let prover_identities ~prefix_common ~prefix ~public:_ ~domain:_ :
          prover_identities =
       fun evaluations ->
        let q_label = prefix_common q_label in
        let output = prefix output in
        let res = Evaluations.find_evaluation evaluations "tmp_eval" in
        let res =
          Evaluations.mul ~res ~evaluations ~poly_names:[ q_label; output ]
            ~powers:[ 1; 5 ] ()
        in
        SMap.singleton (prefix @@ arith ^ ".0") res

      let verifier_identities ~prefix_common ~prefix ~public:_ ~generator:_
          ~size_domain:_ : verifier_identities =
       fun _ answers ->
        let q = get_answer answers X @@ prefix_common q_label in
        let l = get_answer answers X @@ prefix output in
        let l2 = Scalar.mul l l in
        let l4 = Scalar.mul l2 l2 in
        let l5 = Scalar.mul l4 l in
        let res = Scalar.mul q l5 in
        SMap.singleton (prefix @@ arith ^ ".0") res

      let polynomials_degree = SMap.of_list [ (output, 6); (q_label, 6) ]

      let cs ~q:qx5c ~a:_ ~b:_ ~c ~ag:_ ~bg:_ ~cg:_ =
        map_singleton
          L.(
            let* c5 = Num.pow5 c in
            Num.mul qx5c c5)
    end

    (* Add right²
       Arith monomial
       degree : 6n
       advice selectors : None
       equations : + q×b²
    *)
    module X2B : Base_sig = struct
      let q_label = "qx2b"
      let identity = (arith, 1)
      let gx_composition = false

      let equations ~q ~a:_ ~b ~c:_ ~ag:_ ~bg:_ ~cg:_ () =
        Scalar.[ q * square b ]

      let blinds = SMap.singleton right [| 1; 0 |]

      let prover_identities ~prefix_common ~prefix ~public:_ ~domain:_ :
          prover_identities =
       fun evaluations ->
        let q_label = prefix_common q_label in
        let right = prefix right in
        let res = Evaluations.find_evaluation evaluations "tmp_eval" in
        let res =
          Evaluations.mul ~res ~evaluations ~poly_names:[ q_label; right ]
            ~powers:[ 1; 2 ] ()
        in
        SMap.singleton (prefix @@ arith ^ ".0") res

      let verifier_identities ~prefix_common ~prefix ~public:_ ~generator:_
          ~size_domain:_ : verifier_identities =
       fun _ answers ->
        let q = get_answer answers X @@ prefix_common q_label in
        let r = get_answer answers X @@ prefix right in
        let res = Scalar.(q * square r) in
        SMap.singleton (prefix @@ arith ^ ".0") res

      let polynomials_degree = SMap.of_list [ (right, 3); (q_label, 3) ]

      let cs ~q:qx2b ~a:_ ~b ~c:_ ~ag:_ ~bg:_ ~cg:_ =
        map_singleton
          L.(
            let* b2 = Num.square b in
            Num.mul qx2b b2)
    end

    (* Weierstrass elliptic curve addition : checks that (a, ag) + (b, bg) = (c, cg)
       Non Arith
       degree : 4n
       nb identities : 2
       advice selectors : None
       equations : with λ = (bg - ag) / (b - a),
          · q × [ ((a + b + c) × (b - a)²) - (bg - ag)² ] = 0
          · q × [ ((ag + cg) × (b - a)) - (a - c) × (bg - ag) ] = 0
       /!\ q must be 0 or 1
    *)
    module AddWeierstrass : Base_sig = struct
      let q_label = "qecc_ws_add"
      let identity = (q_label, 2)
      let gx_composition = true

      let equations ~q ~a ~b ~c ~ag ~bg ~cg () =
        if Scalar.is_zero q then Scalar.[ zero; zero ]
        else if not (Scalar.(is_one) q) then
          failwith "AddWeierstrass.equations : qecc_ws_add must be zero or one."
        else
          let lambda = Scalar.(div_exn (sub bg ag) (sub b a)) in
          let x = Scalar.(sub (lambda * lambda) (a + b)) in
          let y = Scalar.(sub (lambda * sub a x) ag) in
          Scalar.[ sub x c; sub y cg ]

      let blinds =
        SMap.of_list
          [ (right, [| 1; 1 |]); (left, [| 1; 1 |]); (output, [| 1; 1 |]) ]

      let prover_identities ~prefix_common ~prefix ~public:_ ~domain :
          prover_identities =
       fun evaluations ->
        (* lambda:
           numerator =  [bg - ag] ;
           denominator = [b - a] *)
        (* identity on new point's x coordinate:
           [c + b + a] [b - a]^2 - [bg - ag]^2 = 0 *)
        let q_label = prefix_common q_label in
        let left = prefix left in
        let right = prefix right in
        let output = prefix output in
        let domain_size = Domain.length domain in
        let eval_q_label = Evaluations.find_evaluation evaluations q_label in
        let eval_length = Evaluations.length eval_q_label in
        let id1_evaluation = Evaluations.create eval_length in
        let id2_evaluation = Evaluations.create eval_length in
        let tmp1_evaluation = Evaluations.create eval_length in
        let tmp2_evaluation = Evaluations.create eval_length in
        let tmp3_evaluation = Evaluations.create eval_length in
        (* tmp3_evaluation <- (b - a) *)
        let eval_b_minus_a =
          Evaluations.linear ~res:tmp3_evaluation ~evaluations
            ~poly_names:[ right; left ] ~linear_coeffs:[ one; mone ] ()
        in
        (* tmp2_evaluation <- (b - a)^2 *)
        let eval_b_minus_a_sqr =
          Evaluations.mul_c ~res:tmp2_evaluation ~evaluations:[ eval_b_minus_a ]
            ~powers:[ 2 ] ()
        in
        (* id2_evaluation <- (a + b + c) *)
        let eval_a_plus_b_plus_c =
          Evaluations.linear ~res:id2_evaluation ~evaluations
            ~poly_names:[ left; right; output ] ()
        in
        (* tmp1_evaluation <- (a + b + c)·(b - a)^2 *)
        let eval_left_term =
          Evaluations.mul_c ~res:tmp1_evaluation
            ~evaluations:[ eval_a_plus_b_plus_c; eval_b_minus_a_sqr ]
            ()
        in
        (* id2_evaluation <- (a + b + c)·(b - a)^2 - (bg - ag)^2 *)
        let eval_first_identity =
          Evaluations.linear_c ~res:id2_evaluation
            ~evaluations:[ eval_left_term; eval_b_minus_a_sqr ]
            ~composition_gx:([ 0; 1 ], domain_size)
            ~linear_coeffs:[ one; mone ] ()
        in
        let first_identity =
          Evaluations.mul_c ~res:id1_evaluation
            ~evaluations:[ eval_q_label; eval_first_identity ]
            ()
        in
        (* identity on new point's y coordinate:
           [cg + ag] [b - a] - [bg - ag] [a - c] = 0 *)
        (* id2_evaluation <- (cg + ag) *)
        let eval_cg_plus_ag =
          Evaluations.linear ~res:id2_evaluation ~evaluations
            ~poly_names:[ output; left ]
            ~composition_gx:([ 1; 1 ], domain_size)
            ()
        in
        (* tmp1_evaluation <- (cg + ag)·(b - a) *)
        let eval_left_term_2 =
          Evaluations.mul_c ~res:tmp1_evaluation
            ~evaluations:[ eval_cg_plus_ag; eval_b_minus_a ]
            ()
        in
        (* id2_evaluation <- (a - c) *)
        let eval_a_minus_c =
          Evaluations.linear ~res:id2_evaluation ~evaluations
            ~poly_names:[ left; output ] ~linear_coeffs:[ one; mone ] ()
        in
        (* tmp2_evaluation <- (bg - ag)·(a - c) *)
        let eval_right_term_2 =
          Evaluations.mul_c ~res:tmp2_evaluation
            ~evaluations:[ eval_b_minus_a; eval_a_minus_c ]
            ~composition_gx:([ 1; 0 ], domain_size)
            ()
        in
        (* tmp3_evaluation <- (cg + ag)·(b - a) - (bg - ag)·(a - c) *)
        let eval_second_identity =
          Evaluations.linear_c ~res:tmp3_evaluation
            ~evaluations:[ eval_left_term_2; eval_right_term_2 ]
            ~linear_coeffs:[ one; mone ] ()
        in
        let second_identity =
          Evaluations.mul_c ~res:id2_evaluation
            ~evaluations:[ eval_q_label; eval_second_identity ]
            ()
        in
        SMap.of_list
          [
            (prefix @@ q_label ^ ".0", first_identity);
            (prefix @@ q_label ^ ".1", second_identity);
          ]

      let verifier_identities ~prefix_common ~prefix ~public:_ ~generator:_
          ~size_domain:_ : verifier_identities =
       fun _ answers ->
        let q = get_answer answers X @@ prefix_common q_label in
        let l = get_answer answers X @@ prefix left in
        let r = get_answer answers X @@ prefix right in
        let o = get_answer answers X @@ prefix output in
        let lg = get_answer answers GX @@ prefix left in
        let rg = get_answer answers GX @@ prefix right in
        let og = get_answer answers GX @@ prefix output in
        let num_lambda = Scalar.(sub rg lg) in
        let den_lambda = Scalar.(sub r l) in
        (* identity on new point's x coordinate:
           [o + r + l] [r - l]^2 - [rg - lg]^2 = 0 *)
        let first_identity =
          let num_lambda2 = Scalar.mul num_lambda num_lambda in
          let den_lambda2 = Scalar.mul den_lambda den_lambda in
          let id = Scalar.(sub ((o + r + l) * den_lambda2) num_lambda2) in
          Scalar.mul q id
        in
        (* identity on new point's y coordinate:
           [og + lg] [r - l] - [rg - lg] [l - o] = 0 *)
        let second_identity =
          let id =
            Scalar.(sub ((og + lg) * den_lambda) (num_lambda * sub l o))
          in
          Scalar.mul q id
        in
        SMap.of_list
          [
            (prefix @@ q_label ^ ".0", first_identity);
            (prefix @@ q_label ^ ".1", second_identity);
          ]

      let polynomials_degree =
        SMap.of_list [ (left, 4); (right, 4); (output, 4); (q_label, 4) ]

      let cs ~q:qec ~a ~b ~c ~ag ~bg ~cg =
        let open L in
        let sub = Num.add ~qr:mone in
        let* lambda_num = sub bg ag in
        let* lambda_denom = sub b a in

        let* lambda_num2 = Num.mul lambda_num lambda_num in
        let* cba =
          let* cb = Num.add c b in
          Num.add cb a
        in
        let* fst_term =
          Num.mul_list (to_list [ cba; lambda_denom; lambda_denom ])
        in
        let* fst = sub fst_term lambda_num2 in
        let* fst = Num.mul qec fst in
        let* cgag = Num.add cg ag in
        let* ac = sub a c in
        let* fst_term = Num.mul cgag lambda_denom in
        let* snd_term = Num.mul lambda_num ac in
        let* snd = sub fst_term snd_term in
        let* snd = Num.mul qec snd in
        ret [ fst; snd ]
    end

    (* Edwards elliptic curve addition : checks that (a, ag) + (b, bg) = (c, cg)
       Non Arith
       degree : 6n
       nb identities : 2
       advice selectors : None
       equations :
          · q × [ c×(1 + param_d×a×bg×b×ag) - (a×bg + b×ag) ] = 0
          · q × [ cg×(1 - param_d×a×bg×b×ag) - (bg×ag - param_a×b×a) ] = 0
       /!\ q must be 0 or 1
    *)
    module AddEdwards : Base_sig = struct
      let q_label = "qecc_ed_add"
      let identity = (q_label, 2)
      let gx_composition = true

      (* JubJub curve parameters *)
      let param_a = mone

      let param_d =
        Scalar.of_string
          "19257038036680949359750312669786877991949435402254120286184196891950884077233"

      let equations ~q ~a ~b ~c ~ag ~bg ~cg () =
        if Scalar.is_zero q then Scalar.[ zero; zero ]
        else if not (Scalar.is_one q) then
          failwith "AddEdwards.equations : qecc_ed_add must be zero or one."
        else
          let xpyq = Scalar.(a * bg) in
          let xqyp = Scalar.(b * ag) in
          let ypyq = Scalar.(bg * ag) in
          let xpxq = Scalar.(b * a) in
          let xr = Scalar.((xpyq + xqyp) / (one + (param_d * xpyq * xqyp))) in
          let yr =
            Scalar.(
              (ypyq + (negate param_a * xpxq))
              / (one + (negate param_d * xpyq * xqyp)))
          in
          Scalar.[ xr + negate c; yr + negate cg ]

      let blinds =
        SMap.of_list
          [ (right, [| 1; 1 |]); (left, [| 1; 1 |]); (output, [| 1; 1 |]) ]

      let prover_identities ~prefix_common ~prefix ~public:_ ~domain :
          prover_identities =
       fun evaluations ->
        let domain_size = Domain.length domain in
        (* identity on new point's x coordinate:
           q * [x3 * (1 + Params_d * x1 * x2 * y1 * y2) - (x1 * y2 + y1 * x2)] = 0
           q * [c * (1 + Params_d * a * b * ag * bg) - (a * bg + b * ag)] = 0 *)
        let q_label = prefix_common q_label in
        let left = prefix left in
        let right = prefix right in
        let output = prefix output in
        let eval_c = Evaluations.find_evaluation evaluations output in
        let eval_q_label = Evaluations.find_evaluation evaluations q_label in
        let eval_length = Evaluations.length eval_q_label in
        let id1_evaluation = Evaluations.create eval_length in
        let id2_evaluation = Evaluations.create eval_length in
        let tmp1_evaluation = Evaluations.create eval_length in
        let tmp2_evaluation =
          Evaluations.find_evaluation evaluations "tmp_eval"
        in
        (* tmp1_evaluation <- a·bg *)
        let eval_a_mul_bg =
          Evaluations.mul ~res:tmp1_evaluation ~evaluations
            ~poly_names:[ left; right ]
            ~composition_gx:([ 0; 1 ], domain_size)
            ()
        in
        (* tmp2_evaluation <- b·ag *)
        let eval_b_mul_ag =
          Evaluations.mul ~res:tmp2_evaluation ~evaluations
            ~poly_names:[ left; right ]
            ~composition_gx:([ 1; 0 ], domain_size)
            ()
        in
        (* id1_evaluation <- a·b·ag·bg *)
        let eval_a_mul_b_mul_ag_mul_bg =
          Evaluations.mul_c ~res:id1_evaluation
            ~evaluations:[ eval_a_mul_bg; eval_b_mul_ag ]
            ()
        in
        (* id2_evaluation <- 1 + Params_d·a·b·ag·bg *)
        let eval_1_plus_d_mul_a_mul_b_mul_ag_mul_bg =
          Evaluations.linear_c ~res:id2_evaluation
            ~evaluations:[ eval_a_mul_b_mul_ag_mul_bg ]
            ~linear_coeffs:[ param_d ] ~add_constant:one ()
        in
        (* id1_evaluation <- a·bg + b·ag *)
        let eval_a_mul_bg_plus_b_mul_ag =
          Evaluations.linear_c ~res:id1_evaluation
            ~evaluations:[ eval_a_mul_bg; eval_b_mul_ag ]
            ()
        in
        (* tmp1_evaluation <- c·(1 + Params_d·a·b·ag·bg) *)
        let eval_c_mul_p_1_plus_d_mul_a_mul_b_mul_ag_mul_bg_p =
          Evaluations.mul_c ~res:tmp1_evaluation
            ~evaluations:[ eval_1_plus_d_mul_a_mul_b_mul_ag_mul_bg; eval_c ]
            ()
        in
        (* tmp2_evaluation <- c·(1 + Params_d·a·b·ag·bg) - (a·bg + b·ag) *)
        let eval_first_identity =
          Evaluations.linear_c ~res:tmp2_evaluation
            ~evaluations:
              [
                eval_c_mul_p_1_plus_d_mul_a_mul_b_mul_ag_mul_bg_p;
                eval_a_mul_bg_plus_b_mul_ag;
              ]
            ~linear_coeffs:[ one; mone ] ()
        in
        let first_identity =
          Evaluations.mul_c ~res:id1_evaluation
            ~evaluations:[ eval_q_label; eval_first_identity ]
            ()
        in
        (* identity on new point's y coordinate:
           q * [y3 * (1 - Params_d * x1 * x2 * y1 * y2) - (y1 * y2 - Params_a * x1 * x2)]  = 0
           q * [cg * (1 - Params_d * a * b * ag * bg) - (ag * bg - Params_a * b * a)] = 0
        *)
        (* tmp2_evaluation <- (1 - Params_d·a·b·ag·bg) *)
        let eval_1_minus_d_mul_a_mul_b_mul_ag_mul_bg =
          Evaluations.linear_c ~res:tmp2_evaluation
            ~evaluations:[ eval_1_plus_d_mul_a_mul_b_mul_ag_mul_bg ]
            ~linear_coeffs:[ mone ] ~add_constant:(Scalar.add one one) ()
        in
        (* tmp1_evaluation <- cg·(1 - Params_d·a·b·ag·bg) *)
        let eval_cg_mul_p_1_minus_d_mul_a_mul_b_mul_ag_mul_bg_p =
          Evaluations.mul_c ~res:tmp1_evaluation
            ~evaluations:[ eval_1_minus_d_mul_a_mul_b_mul_ag_mul_bg; eval_c ]
            ~composition_gx:([ 0; 1 ], domain_size)
            ()
        in
        (* id2_evaluation <- a·b *)
        let eval_a_mul_b =
          Evaluations.mul ~res:id2_evaluation ~evaluations
            ~poly_names:[ left; right ] ()
        in
        (* tmp2_evaluation <- cg·(1 - Params_d·a·b·ag·bg) - ag·bg + Params_a·a·b *)
        let eval_second_identity =
          Evaluations.linear_c ~res:tmp2_evaluation
            ~evaluations:
              [
                eval_cg_mul_p_1_minus_d_mul_a_mul_b_mul_ag_mul_bg_p;
                eval_a_mul_b;
                eval_a_mul_b;
              ]
            ~composition_gx:([ 0; 1; 0 ], domain_size)
            ~linear_coeffs:[ one; mone; param_a ] ()
        in
        let second_identity =
          Evaluations.mul_c ~res:id2_evaluation
            ~evaluations:[ eval_q_label; eval_second_identity ]
            ()
        in
        SMap.of_list
          [
            (prefix @@ q_label ^ ".0", first_identity);
            (prefix @@ q_label ^ ".1", second_identity);
          ]

      let verifier_identities ~prefix_common ~prefix ~public:_ ~generator:_
          ~size_domain:_ : verifier_identities =
       fun _ answers ->
        let q = get_answer answers X @@ prefix_common q_label in
        let x1 = get_answer answers X @@ prefix left in
        let y1 = get_answer answers GX @@ prefix left in
        let x2 = get_answer answers X @@ prefix right in
        let y2 = get_answer answers GX @@ prefix right in
        let x3 = get_answer answers X @@ prefix output in
        let y3 = get_answer answers GX @@ prefix output in
        let x1x2 = Scalar.mul x1 x2 in
        let y1y2 = Scalar.mul y1 y2 in
        let den_common = Scalar.(param_d * x1x2 * y1y2) in
        (* q * [x3 * (1 + d * x1 * x2 * y1 * y2) - (x1 * y2 + y1 * x2)] = 0 *)
        let first_identity =
          let num = Scalar.((x1 * y2) + (y1 * x2)) in
          let den = Scalar.(one + den_common) in
          let id = Scalar.(sub (x3 * den) num) in
          Scalar.mul q id
        in
        (* q * [y3 * (1 - d * x1 * x2 * y1 * y2) - (y1*y2 - a * x1 * x2)] = 0 *)
        let second_identity =
          let num = Scalar.(sub y1y2 (param_a * x1x2)) in
          let den = Scalar.(sub one den_common) in
          let id = Scalar.(sub (y3 * den) num) in
          Scalar.mul q id
        in
        SMap.of_list
          [
            (prefix @@ q_label ^ ".0", first_identity);
            (prefix @@ q_label ^ ".1", second_identity);
          ]

      let polynomials_degree =
        SMap.of_list [ (left, 6); (right, 6); (output, 6); (q_label, 6) ]

      let cs ~q:qec ~a ~b ~c ~ag ~bg ~cg =
        let open L in
        let sub = Num.add ~qr:mone in
        (* x_p * y_q *)
        let* abg = Num.mul a bg in
        (* x_q * y_p *)
        let* bag = Num.mul b ag in
        (* d * x_p * x_q * y_p * y_q *)
        let* dabagbg = Num.mul ~qm:param_d abg bag in
        (* 1 + d * x_p * x_q * y_p * y_q *)
        let* fst_term = Num.custom ~ql:one ~qm:one c dabagbg in
        let* snd_term = Num.add abg bag in
        let* fst = sub fst_term snd_term in
        let* fst = Num.mul qec fst in
        (* y_r * (1 - d * x_p * x_q * y_p * y_q) *)
        let* fst_term = Num.custom ~ql:one ~qm:mone cg dabagbg in
        (* y_p * y_q - a * x_p * x_q *)
        let* snd_term =
          (* y_p * y_q *)
          let* agbg = Num.mul ag bg in
          (* a * x_p * x_q *)
          let* ab = Num.mul ~qm:param_a a b in
          (* y_p * y_q - a * x_p * x_q *)
          sub agbg ab
        in
        let* snd = sub fst_term snd_term in
        let* snd = Num.mul qec snd in
        ret [ fst; snd ]
    end

    (* Add public input polynomial
       Arith monomial
       degree : n
       advice selectors : None
       equations : + q×a×b
    *)
    module Public : Base_sig = struct
      let q_label = "qpub"
      let blinds = SMap.empty
      let identity = (arith, 1)
      let gx_composition = false
      let equations ~q:_ ~a:_ ~b:_ ~c:_ ~ag:_ ~bg:_ ~cg:_ () = Scalar.[ zero ]

      let compute_PI ~start public_inputs domain evaluations =
        let size_domain = Domain.length domain in
        if size_domain = 0 then Evaluations.zero
        else
          let l = Array.length public_inputs in
          let scalars =
            Array.(
              concat
                [
                  init start (fun _ -> Scalar.zero);
                  public_inputs;
                  init (size_domain - l - start) (fun _ -> Scalar.zero);
                ])
          in
          let pi =
            Poly.(opposite (Evaluations.interpolation_fft2 domain scalars))
          in
          let domain = Evaluations.get_domain evaluations in
          Evaluations.evaluation_fft domain pi

      let prover_identities ~prefix_common:_ ~prefix ~public ~domain :
          prover_identities =
       fun evaluations ->
        let res =
          compute_PI ~start:public.input_coms_size public.public_inputs domain
            evaluations
        in
        SMap.singleton (prefix @@ arith ^ ".0") res

      let verifier_identities ~prefix_common:_ ~prefix ~public ~generator
          ~size_domain : verifier_identities =
       fun x _ ->
        let res =
          if size_domain = 0 then Scalar.zero
          else
            let g = Scalar.inverse_exn generator in
            let f (acc, gix) wi =
              let den = Scalar.(sub gix one) in
              Scalar.(acc + (wi / den), g * gix)
            in
            let res, _ =
              let shift = public.input_coms_size in
              let gx_init = Scalar.(pow generator Z.(neg (of_int shift)) * x) in
              Array.fold_left f Scalar.(zero, gx_init) public.public_inputs
            in
            let n = size_domain in
            let xn = Scalar.pow x (Z.of_int n) in
            let xn_min_1_div_n = Scalar.(sub xn one / of_int n) in
            Scalar.(negate (xn_min_1_div_n * res))
        in
        SMap.singleton (prefix @@ arith ^ ".0") res

      let polynomials_degree = SMap.empty

      (* this function will not be used *)
      let cs ~q:_ ~a:_ ~b:_ ~c:_ ~ag:_ ~bg:_ ~cg:_ =
        let open L in
        ret []
    end

    (* Add idx-th input com polynomial
       Arith monomial
       degree : 2n
       advice selectors : None
       equations : + q×com_idx
    *)
    module InputCom (Com : sig
      val idx : int
    end) : Base_sig = struct
      let q_label = "qcom" ^ string_of_int Com.idx
      let com_label = com_label ^ string_of_int Com.idx
      let blinds = SMap.empty
      let identity = (arith, 1)
      let gx_composition = false
      let equations ~q:_ ~a:_ ~b:_ ~c:_ ~ag:_ ~bg:_ ~cg:_ () = Scalar.[ zero ]

      let prover_identities ~prefix_common ~prefix ~public:_ ~domain:_ :
          prover_identities =
       fun evaluations ->
        let res = Evaluations.find_evaluation evaluations "tmp_eval" in
        let res =
          Evaluations.mul ~res ~evaluations
            ~poly_names:[ prefix_common q_label; prefix com_label ]
            ()
        in
        SMap.singleton (prefix @@ arith ^ ".0") res

      let verifier_identities ~prefix_common ~prefix ~public:_ ~generator:_
          ~size_domain:_ : verifier_identities =
       fun _ answers ->
        let q = get_answer answers X @@ prefix_common q_label in
        let com = get_answer answers X @@ prefix com_label in
        let res = Scalar.(q * com) in
        SMap.singleton (prefix @@ arith ^ ".0") res

      let polynomials_degree = SMap.of_list [ (com_label, 2); (q_label, 2) ]

      (* TODO: implement *)
      let cs ~q:_ ~a:_ ~b:_ ~c:_ ~ag:_ ~bg:_ ~cg:_ =
        failwith "input commitments in aPlonK proofs are not supported yet"
    end
  end

  let arith_label = Gates.arith
  let com_label = Gates.com_label

  (* Maximum number of input commitments that can be used in a proof *)
  let nb_input_com = 3

  let gates_map =
    let open Gates in
    SMap.of_list
      ([
         (Public.q_label, (module Public : Base_sig));
         (Constant.q_label, (module Constant));
         (AddLeft.q_label, (module AddLeft));
         (AddRight.q_label, (module AddRight));
         (AddOutput.q_label, (module AddOutput));
         (AddNextLeft.q_label, (module AddNextLeft));
         (AddNextRight.q_label, (module AddNextRight));
         (AddNextOutput.q_label, (module AddNextOutput));
         (Multiplication.q_label, (module Multiplication));
         (X5A.q_label, (module X5A));
         (X5C.q_label, (module X5C));
         (X2B.q_label, (module X2B));
         (AddWeierstrass.q_label, (module AddWeierstrass));
         (AddEdwards.q_label, (module AddEdwards));
       ]
      @ List.init nb_input_com (fun i ->
            ( "qcom" ^ string_of_int i,
              (module InputCom (struct
                let idx = i
              end) : Base_sig) )))

  let gates_list = SMap.keys gates_map
  let nb_custom_gates = SMap.cardinal gates_map

  (* Removes non-custom-gates from gates (for example q_plookup) *)
  let filter_gates gates = SMap.(filter (fun q _ -> mem q gates_map) gates)

  let find_gate q =
    match SMap.find_opt q gates_map with
    | Some gate -> gate
    | None ->
        failwith
          (Printf.sprintf "\nCustom_gates.find_gate : unknown selector %s." q)

  let get_blinds q =
    let module M = (val find_gate q : Gates.Base_sig) in
    M.blinds

  let get_prover_identities q =
    let module M = (val find_gate q : Gates.Base_sig) in
    M.prover_identities

  let get_verifier_identities q =
    let module M = (val find_gate q : Gates.Base_sig) in
    M.verifier_identities

  let get_polynomials_degree q =
    let module M = (val find_gate q : Gates.Base_sig) in
    M.polynomials_degree

  let get_eqs q =
    let module M = (val find_gate q : Gates.Base_sig) in
    M.equations

  let get_ids q =
    let module M = (val find_gate q : Gates.Base_sig) in
    M.identity

  let get_cs q =
    let module M = (val find_gate q : Gates.Base_sig) in
    M.cs

  let get_gx_composition q =
    let module M = (val find_gate q : Gates.Base_sig) in
    M.gx_composition

  let aggregate_blinds ~gates =
    let f_union _key a1 a2 =
      if Array.length a1 <> Array.length a2 then
        raise (Invalid_argument "All blinds arrays must have the same size.")
      else Some Array.(init (length a1) (fun i -> max a1.(i) a2.(i)))
    in
    let blinds_array =
      SMap.(
        fold
          (fun gate _ acc_blinds -> union f_union acc_blinds (get_blinds gate))
          (filter_gates gates) empty)
    in
    let sum_array a = Array.fold_left ( + ) 0 a in
    SMap.map sum_array blinds_array

  let aggregate_prover_identities ?(circuit_name = "") ~input_coms_size
      ~proof_idx ~nb_proofs ~gates ~public_inputs ~domain () : prover_identities
      =
    let prefix_common = SMap.Aggregation.add_prefix circuit_name in
    let prefix =
      SMap.Aggregation.add_prefix ~n:nb_proofs ~i:proof_idx circuit_name
    in
    fun evaluations ->
      let size_eval = Evaluations.size_evaluations evaluations in
      let tmp_evaluation = Evaluations.create size_eval in
      let arith_acc_evaluation = Evaluations.create size_eval in
      let evaluations = evaluations |> SMap.add "tmp_eval" tmp_evaluation in
      let init_ids =
        SMap.singleton (prefix @@ Gates.arith ^ ".0") arith_acc_evaluation
      in
      let union key e1 e2 =
        assert (key = prefix @@ Gates.arith ^ ".0");
        Some (Evaluations.add ~res:e1 e1 e2)
      in
      let public = { public_inputs; input_coms_size } in
      SMap.fold
        (fun gate _ accumulated_ids ->
          let gate_ids =
            get_prover_identities gate ~prefix_common ~prefix ~public ~domain
          in
          SMap.union union accumulated_ids (gate_ids evaluations))
        (filter_gates gates) init_ids

  let aggregate_verifier_identities ?(circuit_name = "") ~input_com_sizes
      ~proof_idx ~nb_proofs ~gates ~public_inputs ~generator ~size_domain () :
      verifier_identities =
    let prefix_common = SMap.Aggregation.add_prefix circuit_name in
    let prefix =
      SMap.Aggregation.add_prefix ~n:nb_proofs ~i:proof_idx circuit_name
    in
    fun x answers ->
      let init_ids =
        SMap.singleton (prefix @@ Gates.arith ^ ".0") Scalar.zero
      in
      let union key s1 s2 =
        assert (key = prefix @@ Gates.arith ^ ".0");
        Some (Scalar.add s1 s2)
      in
      let public =
        {
          public_inputs;
          input_coms_size = List.fold_left ( + ) 0 input_com_sizes;
        }
      in
      SMap.fold
        (fun gate _ accumulated_ids ->
          let gate_ids =
            get_verifier_identities gate ~prefix_common ~prefix ~public
              ~generator ~size_domain
          in
          SMap.union union accumulated_ids (gate_ids x answers))
        (filter_gates gates) init_ids

  let aggregate_polynomials_degree ~gates =
    SMap.fold
      (fun gate _ degree ->
        let map = get_polynomials_degree gate in
        SMap.fold (fun _ d acc -> max d acc) map degree)
      (filter_gates gates) 0

  let exists_gx_composition ~gates =
    SMap.exists (fun q _ -> get_gx_composition q) (filter_gates gates)

  (* - (x^n -1)/n * (w0/(x -1) + w1/(x/g - 1) + … + wn/(x/g^n - 1)) *)
  let cs_pi ~generator ~n ~x ~zs pi_list =
    let open L in
    match pi_list with
    | [] -> constant_scalar Scalar.zero
    | hd :: tl_pi ->
        (* negate because we want -PI(x) *)
        let n_inv = Scalar.(inverse_exn (negate n)) in
        let* left_term = Num.mul_by_constant n_inv zs in
        let g_inv = Scalar.inverse_exn generator in
        let* init_value =
          let* x_minus_one = Num.add_constant Scalar.(negate one) x in
          Num.div hd x_minus_one
        in
        let* sum, _g_inv_k =
          foldM
            (fun (res, g_inv_k) w_k ->
              let* x_g_inv_k_minus_one =
                Num.add_constant ~ql:g_inv_k Scalar.(negate one) x
              in
              let* to_add = Num.div w_k x_g_inv_k_minus_one in
              let* res = Num.add res to_add in
              ret (res, Scalar.mul g_inv_k g_inv))
            (init_value, g_inv) tl_pi
        in
        Num.mul left_term sum
end

module type S = sig
  val arith_label : string
  val com_label : string
  val gates_list : string list
  val nb_custom_gates : int
  val nb_input_com : int

  val get_eqs :
    string ->
    q:Scalar.t ->
    a:Scalar.t ->
    b:Scalar.t ->
    c:Scalar.t ->
    ag:Scalar.t ->
    bg:Scalar.t ->
    cg:Scalar.t ->
    unit ->
    Scalar.t list

  val get_ids : string -> string * int

  val get_cs :
    string ->
    q:L.scalar L.repr ->
    a:L.scalar L.repr ->
    b:L.scalar L.repr ->
    c:L.scalar L.repr ->
    ag:L.scalar L.repr ->
    bg:L.scalar L.repr ->
    cg:L.scalar L.repr ->
    L.scalar L.repr list L.t

  val aggregate_blinds : gates:'a SMap.t -> int SMap.t

  val aggregate_prover_identities :
    ?circuit_name:string ->
    input_coms_size:int ->
    proof_idx:int ->
    nb_proofs:int ->
    gates:'a SMap.t ->
    public_inputs:Scalar.t array ->
    domain:Domain.t ->
    unit ->
    prover_identities

  val aggregate_verifier_identities :
    ?circuit_name:string ->
    input_com_sizes:int list ->
    proof_idx:int ->
    nb_proofs:int ->
    gates:'a SMap.t ->
    public_inputs:Scalar.t array ->
    generator:Scalar.t ->
    size_domain:int ->
    unit ->
    verifier_identities

  val aggregate_polynomials_degree : gates:'a SMap.t -> int
  val exists_gx_composition : gates:'a SMap.t -> bool

  val cs_pi :
    generator:Scalar.t ->
    n:Scalar.t ->
    x:L.scalar L.repr ->
    zs:L.scalar L.repr ->
    L.scalar L.repr list ->
    L.scalar L.repr L.t
end

include (Aggregator : S)
