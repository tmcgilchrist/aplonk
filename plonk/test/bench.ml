(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Main = Plonk.Main_protocol
module H = Plonk_test.Helpers.Make (Main)

let () =
  let k = 16 in
  let public_input_size = 10 in
  let start_build_circuit = Unix.gettimeofday () in
  let case =
    Plonk_test.Cases.Big_circuit.make ~nb_proofs:1 ~public_input_size ~k
    |> List.hd
  in
  let end_build_circuit = Unix.gettimeofday () in
  Printf.printf "\n\nNumber of gates : %d\nNumber of public inputs : %d\n"
    ((1 lsl k) - 1)
    public_input_size;
  Printf.printf "Dummy circuit built in %f ms.\n"
    ((end_build_circuit -. start_build_circuit) *. 1000.);
  H.run_test_case ~verbose:true ~zero_knowledge:false case ()
