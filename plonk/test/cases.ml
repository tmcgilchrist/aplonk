open Plonk
module Scalar = Bls.Scalar

let ( ! ) = List.map Scalar.of_int
let ( !! ) l = List.map Scalar.of_int l |> Array.of_list

type outcome = Valid | Proof_error | Lookup_error

type circuit = {
  wires : int list SMap.t;
  gates : Scalar.t list SMap.t;
  tables : Scalar.t array list list;
  public_input_size : int;
  input_com_sizes : int list;
}

type case = {
  name : string;
  circuit : circuit;
  witness : Scalar.t array;
  outcome : outcome;
}

(* This function aggregates cases ; if several cases concern the same circuit,
   it becomes a circuit with several statements *)
let aggregate_cases cases =
  let outcome = (List.hd cases).outcome in
  let name, circuits_map, inputs_map, outcome =
    List.fold_left
      (fun (name, circuits_map, inputs_map, outcome) case ->
        assert (outcome = case.outcome);
        let circuit =
          let { gates; wires; tables; public_input_size; input_com_sizes } =
            case.circuit
          in
          Circuit.make ~gates ~wires ~tables ~public_input_size ~input_com_sizes
            ()
        in
        let other_inputs =
          Option.value ~default:[] @@ SMap.find_opt case.name inputs_map
        in
        let inputs = case.witness :: other_inputs in
        SMap.
          ( name ^ "+" ^ case.name,
            add case.name (circuit, List.length inputs) circuits_map,
            add case.name inputs inputs_map,
            outcome ))
      SMap.("", empty, empty, outcome)
      cases
  in
  let inputs_map = SMap.map List.rev inputs_map in
  (name, circuits_map, inputs_map, outcome)

let set_circuit ?(tables = []) ?(public_input_size = 0) ?(input_com_sizes = [])
    wires gates =
  { wires; gates; tables; public_input_size; input_com_sizes }

module Unit_tests_for_each_selector = struct
  (* ---- Unit tests for each selector. ----
     We make circuits with 2 constraints,
     as circuits with 1 constraint are not supported. *)

  let qc =
    let name = "qc" in
    let witness = !![ 0; 1 ] in
    let circuit =
      let wires = Circuit.make_wires ~a:[ 0; 0 ] ~b:[ 0; 0 ] ~c:[ 0; 1 ] () in
      let gates = Circuit.make_gates ~qo:![ 0; -1 ] ~qc:![ 0; 1 ] () in
      set_circuit wires gates
    in
    { name; circuit; witness; outcome = Valid }

  let ql =
    let name = "ql" in
    let witness = !![ 0; 1 ] in
    let circuit =
      let wires = Circuit.make_wires ~a:[ 0; 1 ] ~b:[ 0; 0 ] ~c:[ 0; 1 ] () in
      let gates = Circuit.make_gates ~ql:![ 1; 1 ] ~qo:![ 0; -1 ] () in
      set_circuit wires gates
    in
    { name; circuit; witness; outcome = Valid }

  let qr =
    let name = "qr" in
    let witness = !![ 0; 1 ] in
    let circuit =
      let wires = Circuit.make_wires ~a:[ 0; 0 ] ~b:[ 0; 1 ] ~c:[ 0; 1 ] () in
      let gates = Circuit.make_gates ~qr:![ 1; 1 ] ~qo:![ 0; -1 ] () in
      set_circuit wires gates
    in
    { name; circuit; witness; outcome = Valid }

  let qlg =
    let name = "qlg" in
    let witness = !![ 0; 1 ] in
    let circuit =
      let wires = Circuit.make_wires ~a:[ 0; 1 ] ~b:[ 0; 0 ] ~c:[ 1; 0 ] () in
      let gates = Circuit.make_gates ~qlg:![ 1; 0 ] ~qo:![ -1; 0 ] () in
      set_circuit wires gates
    in
    { name; circuit; witness; outcome = Valid }

  let qrg =
    let name = "qrg" in
    let witness = !![ 0; 1 ] in
    let circuit =
      let wires = Circuit.make_wires ~a:[ 0; 0 ] ~b:[ 0; 1 ] ~c:[ 1; 0 ] () in
      let gates = Circuit.make_gates ~qrg:![ 1; 0 ] ~qo:![ -1; 0 ] () in
      set_circuit wires gates
    in
    { name; circuit; witness; outcome = Valid }

  let qog =
    let name = "qog" in
    let witness = !![ 0; 1 ] in
    let circuit =
      let wires = Circuit.make_wires ~a:[ 0; 0 ] ~b:[ 0; 0 ] ~c:[ 1; 1 ] () in
      let gates = Circuit.make_gates ~qog:![ 1; 0 ] ~qo:![ -1; 0 ] () in
      set_circuit wires gates
    in
    { name; circuit; witness; outcome = Valid }

  let qm =
    let name = "qm" in
    let witness = !![ 0; 1 ] in
    let circuit =
      let wires = Circuit.make_wires ~a:[ 0; 1 ] ~b:[ 1; 1 ] ~c:[ 0; 1 ] () in
      let gates = Circuit.make_gates ~qm:![ 1; 1 ] ~qo:![ 0; -1 ] () in
      set_circuit wires gates
    in
    { name; circuit; witness; outcome = Valid }

  let qx2b =
    let name = "qx2b" in
    let witness = !![ 0; -3; 3; 9 ] in
    let circuit =
      let wires =
        Circuit.make_wires ~a:[ 0; 0; 0 ] ~b:[ 0; 1; 2 ] ~c:[ 0; 3; 3 ] ()
      in
      let gates = Circuit.make_gates ~qx2b:![ 1; 1; 1 ] ~qo:![ 0; -1; -1 ] () in
      set_circuit wires gates
    in
    { name; circuit; witness; outcome = Valid }

  let qx5a =
    let name = "qx5a" in
    let witness = !![ 0; 2; 32 ] in
    let circuit =
      let wires = Circuit.make_wires ~a:[ 0; 1 ] ~b:[ 0; 0 ] ~c:[ 0; 2 ] () in
      let gates = Circuit.make_gates ~qx5a:![ 1; 1 ] ~qo:![ 0; -1 ] () in
      set_circuit wires gates
    in
    { name; circuit; witness; outcome = Valid }

  let qx5c =
    let name = "qx5c" in
    let witness = !![ 0; 243; 3 ] in
    let circuit =
      let wires = Circuit.make_wires ~a:[ 0; 1 ] ~b:[ 0; 0 ] ~c:[ 0; 2 ] () in
      let gates = Circuit.make_gates ~qx5c:![ 1; 1 ] ~ql:![ 0; -1 ] () in
      set_circuit wires gates
    in
    { name; circuit; witness; outcome = Valid }

  let qecc_ws_add =
    let name = "qecc_ws_add" in
    (* We check that: (3,1) + (4,4) = (2,2).
       These are dummy points, they do not belong to a specific curve. *)
    let witness = !![ 1; 2; 3; 4 ] in
    let circuit =
      let wires = Circuit.make_wires ~a:[ 2; 0 ] ~b:[ 3; 3 ] ~c:[ 1; 1 ] () in
      let gates = Circuit.make_gates ~qecc_ws_add:![ 1; 0 ] () in
      set_circuit wires gates
    in
    { name; circuit; witness; outcome = Valid }

  let qecc_ed_add =
    let name = "qecc_ed_add" in
    let witness = !![ 0; 1 ] in
    let circuit =
      let wires = Circuit.make_wires ~a:[ 0; 1 ] ~b:[ 0; 1 ] ~c:[ 0; 1 ] () in
      let gates = Circuit.make_gates ~qecc_ed_add:![ 1; 0 ] () in
      set_circuit wires gates
    in
    { name; circuit; witness; outcome = Valid }

  let list =
    [
      qc; ql; qr; qlg; qrg; qog; qm; qx2b; qx5a; qx5c; qecc_ws_add; qecc_ed_add;
    ]
end

module General_circuits = struct
  let bnot =
    let name = "bnot" in
    let witness = !![ 0; 1 ] in
    let circuit =
      let wires = Circuit.make_wires ~a:[ 0; 0 ] ~b:[ 0; 0 ] ~c:[ 1; 0 ] () in
      let gates = Circuit.make_gates ~qo:![ -1; -1 ] ~qc:![ 1; 0 ] () in
      set_circuit ~public_input_size:1 wires gates
    in
    { name; circuit; witness; outcome = Valid }

  let list = [ bnot ]
end

module General = struct
  (*  General tests *)

  (* Proving the relations
       x10 = x0 + x1 * (x2 + x3 * (x4 + x5))
       & P(x2, x0) + Q(x3, x3) = R(x1, x1)

       Using intermediary variables:
       x10 = x0 + x1 * (x2 + x3 * x6)
       x10 = x0 + x1 * (x2 + x7)
       x10 = x0 + x1 * x8
       x10 = x0 + x9
     <=>
     Constraints:
       1*x0 + 1*x9 - 1*x10 + 0*x0*x9 + 0 = 0
       0*x1 + 0*x8 - 1*x9  + 1*x1*x8 + 0 = 0
       1*x2 + 1*x7 - 1*x8  + 0*x2*x7 + 0 = 0
       0*x3 + 0*x6 - 1*x7  + 1*x3*x6 + 0 = 0
       1*x4 + 1*x5 - 1*x6  + 0*x4*x5 + 0 = 0
       F_add_weirestrass(x2, x3, x1, x0, x3, x1) = 0
  *)

  (* Base circuit proves that:
      95 = 1 + 2 * (3 + 4 * (5 + 6))
      with 1 public input
  *)

  let circuit =
    let wires =
      Circuit.make_wires ~a:[ 0; 1; 2; 3; 4; 2; 0 ] ~b:[ 9; 8; 7; 6; 5; 3; 3 ]
        ~c:[ 10; 9; 8; 7; 6; 1; 1 ] ()
    in
    let gates =
      Circuit.make_gates
        ~ql:![ 1; 0; 1; 0; 1; 0; 0 ]
        ~qr:![ 1; 0; 1; 0; 1; 0; 0 ]
        ~qo:![ -1; -1; -1; -1; -1; 0; 0 ]
        ~qm:![ 0; 1; 0; 1; 0; 0; 0 ]
        ~qecc_ws_add:![ 0; 0; 0; 0; 0; 1; 0 ]
        ()
    in
    set_circuit ~public_input_size:1 wires gates

  let witness = !![ 1; 2; 3; 4; 5; 6; 11; 44; 47; 94; 95 ]

  let zero_values =
    let name = "zero_values" in
    let witness = Array.make 11 (Scalar.of_int 0) in
    { name; circuit; witness; outcome = Valid }

  let non_zero_values =
    let name = "non_zero_values" in
    { name; circuit; witness; outcome = Valid }

  (* Same test with no public inputs *)
  let no_public_inputs =
    let name = "no_public_inputs" in
    let circuit = { circuit with public_input_size = 0 } in
    { name; circuit; witness; outcome = Valid }

  let wrong_values =
    let name = "wrong_values" in
    let witness =
      !![ 1; 2; 3; 4; 5; 6; 11; 44; 47; 94; 94 ]
      (* """mistake""" here *)
    in
    { name; circuit; witness; outcome = Proof_error }

  let input_com =
    let name = "input_commitment" in
    let circuit = { circuit with input_com_sizes = [ 3; 1 ] } in
    { name; circuit; witness; outcome = Valid }

  let list =
    [ zero_values; non_zero_values; no_public_inputs; wrong_values; input_com ]

  let list_one_public_input = [ zero_values; non_zero_values; wrong_values ]
end

module Big_circuit = struct
  (* generates circuit with 2^k - 1 constraints that multiplies 2^k inputs 2 by 2,
     then adds 2^(k-1) inputs 2 by 2, etc, and adds a scalar at each gate ; there
     is a total of k-1 layers of gates, i-th layer contains 2^i gates (starting
     from the "output layer", numbering from 0 to i-1)
     IMPORTANT : with aPlonK, this case is intended to fit PI_rollup_example
     module, which means 2 public inputs and the first element of each witness
     equal to the second element of the previous witness.
  *)
  let make ~nb_proofs ~public_input_size ~k =
    let name = Format.sprintf "big_circuit.%i.%i" public_input_size k in
    let witness nb_proofs k qc =
      let rec build_w (acc_w_left, acc_w_right) =
        let l = List.hd acc_w_left in
        let r = List.hd acc_w_right in
        let len_l = Array.length l in
        assert (Array.(len_l = length r));
        let op =
          if List.length acc_w_left mod 2 = 0 then Scalar.add else Scalar.mul
        in
        let add_qc i =
          let i_constraint =
            let lens =
              List.(fold_left (fun i l -> i + Array.length l))
                0 (acc_w_left @ acc_w_right)
            in
            lens - (1 lsl k) + i
          in
          Scalar.(add qc.(i_constraint))
        in
        if len_l = 1 then
          let last = add_qc 0 (op l.(0) r.(0)) in
          Array.(
            concat List.(rev acc_w_left @ rev acc_w_right @ [ [| last |] ]))
        else
          let wl =
            Array.init (len_l / 2) (fun i ->
                add_qc (2 * i) (op l.(2 * i) r.(2 * i)))
          in
          let wr =
            Array.init (len_l / 2) (fun i ->
                add_qc ((2 * i) + 1) (op l.((2 * i) + 1) r.((2 * i) + 1)))
          in
          build_w (wl :: acc_w_left, wr :: acc_w_right)
      in
      let size = 1 lsl (k - 1) in
      let open Stdlib (* to recover the ! deref operator *) in
      let w0 = ref (Scalar.random ()) in
      List.init nb_proofs (fun _ ->
          let w =
            let w_left =
              Array.init size (fun i -> if i = 0 then !w0 else Scalar.random ())
            in
            let w_right = Array.init size (fun _ -> Scalar.random ()) in
            build_w ([ w_left ], [ w_right ])
          in
          w0 := w.(1);
          w)
    in
    let n = (1 lsl k) - 1 in
    let qc = List.init n (fun _ -> Scalar.random ()) in
    let circuit =
      let wires =
        let a = List.init n Fun.id in
        let b = List.init n (fun i -> n + i) in
        let c =
          List.init (n / 2) (fun i ->
              let i = (1 lsl (k - 1)) + i in
              [ i; n + i ])
          @ [ [ 2 * n ] ]
          |> List.concat
        in
        Circuit.Circuit.make_wires ~a ~b ~c ()
      in
      let gates =
        let is_add k n i =
          let dist = k - Z.(log2 (of_int (Int.sub n i))) in
          if dist mod 2 = 0 then true else false
        in
        let qm = List.init n (fun i -> if is_add k n i then 0 else 1) in
        let ql = List.init n (fun i -> if is_add k n i then 1 else 0) in
        let qr = ql in
        let qo = List.init n (fun _ -> -1) in
        Circuit.Circuit.make_gates ~qm:!qm ~ql:!ql ~qr:!qr ~qo:!qo ~qc ()
      in
      set_circuit ~public_input_size wires gates
    in
    let witnesses = witness nb_proofs k (Array.of_list qc) in
    List.map
      (fun witness -> { name; circuit; witness; outcome = Valid })
      witnesses

  let list = make ~nb_proofs:2 ~public_input_size:2 ~k:5
  let list_slow = make ~nb_proofs:2 ~public_input_size:2 ~k:16
end

let list =
  Unit_tests_for_each_selector.list @ General_circuits.list @ General.list
  @ Big_circuit.list

let list_slow = Big_circuit.list_slow

module Lookup = struct
  (* Tables corresponding to addition of digits mod m to perform tests on. *)
  let table_add_mod_m m =
    let m2 = m * m in
    let t_1 = List.init m2 (fun i -> i / m) in
    let t_2 = List.init m2 (fun i -> i mod m) in
    let t_3 = List.init m2 (fun i -> ((i / m) + i) mod m) in
    [ !!t_1; !!t_2; !!t_3 ]

  let table_add_mod_5 = table_add_mod_m 5
  let table_add_mod_10 = table_add_mod_m 10

  (* ---- Unit tests for each selector. ---- *)
  let qplookup =
    let name = "qplookup" in
    let witness = !![ 0; 1; 2 ] in
    let circuit =
      let wires = Circuit.make_wires ~a:[ 1 ] ~b:[ 1 ] ~c:[ 2 ] () in
      let gates = Circuit.make_gates ~q_plookup:![ 1 ] ~q_table:![ 0 ] () in
      let tables = [ table_add_mod_5 ] in
      set_circuit ~tables wires gates
    in
    { name; circuit; witness; outcome = Valid }

  (* ---- Test with 2 tables. ---- *)
  let qplookup_two_tables =
    let name = "qplookup_two_tables" in
    let witness = !![ 0; 1; 3; 9 ] in
    let circuit =
      let wires = Circuit.make_wires ~a:[ 2; 3 ] ~b:[ 2; 1 ] ~c:[ 1; 0 ] () in
      let gates =
        Circuit.make_gates ~q_plookup:![ 1; 1 ] ~q_table:![ 0; 1 ] ()
      in
      let tables = [ table_add_mod_5; table_add_mod_10 ] in
      set_circuit ~tables wires gates
    in
    { name; circuit; witness; outcome = Valid }

  (* ---- General test with correct and incorrect witness. ---- *)
  let circuit =
    (* Proving the relations with addition mod 5 using lookups
          x8 = x7 + x1 * (x3 + x4 * (x5 + x6))
          R(x2, x2) = P(x3, x1) + Q(x4, x4) <- these are dummy points
          with 1 public input
       <=>
       Constraints:
          lookup: x1 (+) x7 = x8
          1*x1*x7 - 1*x7 = 0
          lookup: x3 (+) x4 = x7
          1*x4*x1 - x4 = 0
          lookup: x5 (+) x6 = x1
          F_add_weirestrass(x3, x4, x2, x1, x4, x2) = 0
    *)
    let wires =
      Circuit.make_wires ~a:[ 1; 1; 4; 2; 4; 3; 1 ] ~b:[ 2; 1; 2; 2; 3; 4; 4 ]
        ~c:[ 3; 1; 1; 4; 2; 2; 2 ] ()
    in
    let gates =
      Circuit.make_gates
        ~qo:![ 0; -1; 0; -1; 0; 0; 0 ]
        ~qm:![ 0; 1; 0; 1; 0; 0; 0 ]
        ~qecc_ws_add:![ 0; 0; 0; 0; 0; 1; 0 ]
        ~q_plookup:![ 1; 0; 1; 0; 1; 0; 0 ]
        ~q_table:![ 0; 0; 0; 0; 0; 0; 0 ]
        ()
    in
    let tables = [ table_add_mod_5 ] in
    set_circuit ~tables ~public_input_size:1 wires gates

  (* Base witness proves that:
      3 = 2 + 1 * (2 + 2 * (3 + 4))   addition modulo 5
      R(2,2) = P(3,1) + Q(4,4) weierstrass point addition
  *)
  let witness = !![ 0; 1; 2; 3; 4 ]

  let lookup_zero_values =
    let name = "lookup_zero_values" in
    let witness = !!(List.init 5 (fun _i -> 0)) in
    { name; circuit; witness; outcome = Valid }

  let lookup_non_zero_values =
    let name = "lookup_non_zero_values" in
    { name; circuit; witness; outcome = Valid }

  let lookup_no_public_inputs =
    let name = "lookup_no_public_inputs" in
    let circuit = { circuit with public_input_size = 0 } in
    { name; circuit; witness; outcome = Valid }

  let lookup_wrong_arith_values =
    let name = "lookup_wrong_arith_values" in
    let wires =
      Circuit.make_wires ~a:[ 1; 1; 4; 2; 4; 3; 1 ] ~b:[ 2; 1; 2; 2; 3; 4; 4 ]
        ~c:[ 3; 1; 1; 3; 2; 2; 2 ] ()
      (* """mistake""" here in arith. constraint *)
    in
    let circuit = { circuit with wires } in
    { name; circuit; witness; outcome = Proof_error }

  let wrong_plookup_values =
    let name = "wrong_plookup_values" in
    let wires =
      Circuit.make_wires ~a:[ 0; 1; 4; 2; 4; 3; 1 ] ~b:[ 2; 1; 2; 2; 3; 4; 4 ]
        ~c:[ 3; 1; 1; 4; 2; 2; 2 ] ()
      (* """mistake""" here in lookup constraint *)
    in
    let circuit = { circuit with wires } in
    { name; circuit; witness; outcome = Lookup_error }

  let list =
    [
      qplookup;
      qplookup_two_tables;
      lookup_zero_values;
      lookup_non_zero_values;
      lookup_no_public_inputs;
      lookup_wrong_arith_values;
      wrong_plookup_values;
    ]
end
