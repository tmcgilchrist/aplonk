open Bls

module type S = sig
  module PC : Polynomial_commitment.S

  type public_parameters = PC.Public_parameters.prover

  type prover_aux = { poly : Poly.t; pc_prover_aux : PC.Commitment.prover_aux }
  [@@deriving repr]

  type public = PC.Commitment.t [@@deriving repr]
  type t = { public : public; prover_aux : prover_aux } [@@deriving repr]

  (* size is the expected length of the commitment ; it must be at least bigger
     than the lenght of the secret
     if it’s given, the secret is padded with zero to reach this length
  *)
  val commit :
    ?size:int -> ?shift:int -> public_parameters -> int -> Scalar.t array -> t
end

module Make_impl (PC : Polynomial_commitment.S) = struct
  module PC = PC

  type public_parameters = PC.Public_parameters.prover

  type prover_aux = { poly : Poly.t; pc_prover_aux : PC.Commitment.prover_aux }
  [@@deriving repr]

  type public = PC.Commitment.t [@@deriving repr]
  type t = { public : public; prover_aux : prover_aux } [@@deriving repr]

  let commit ?size ?(shift = 0) pp n secret =
    let domain = Domain.build n in
    let l = Array.length secret in
    let size = Option.value ~default:l size in
    let secret =
      Array.(append secret (init (size - l) (Fun.const Scalar.zero)))
    in
    (* we add some randomness to hide the secret *)
    let secret =
      let random _ = Scalar.random () in
      let head = Array.init shift random in
      let tail = Array.init (n - size - shift) random in
      Array.concat [ head; secret; tail ]
    in
    let poly = Evaluations.interpolation_fft2 domain secret in
    let poly_map = SMap.singleton "com" poly in
    let public, pc_prover_aux = PC.Commitment.commit pp poly_map in
    { public; prover_aux = { poly; pc_prover_aux } }
end

module Make : functor (PC : Polynomial_commitment.S) -> S with module PC = PC =
  Make_impl
