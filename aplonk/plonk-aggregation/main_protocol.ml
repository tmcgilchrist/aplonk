(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Plonk.Bls
open Plonk.Utils
module SMap = Plonk.SMap

module Make_impl (Super_PP : Polynomial_protocol.S) = struct
  include Plonk.Main_protocol.Make_impl (Super_PP)
  module PP = Super_PP

  type prover_aux = {
    answers : scalar SMap.t SMap.t list;
    batch : scalar SMap.t list;
    alpha : scalar;
    beta : scalar;
    gamma : scalar;
    delta : scalar;
    x : scalar;
    r : scalar;
    cm_answers : PP.Answers_commitment.t;
    cm_pi : PP.Answers_commitment.t;
    t_answers : scalar list;
  }

  type verifier_aux = {
    alpha : scalar;
    beta : scalar;
    gamma : scalar;
    delta : scalar;
    x : scalar;
    r : scalar;
  }

  type input_commit_info = {
    nb_max_answers : int;
    nb_max_pi : int;
    func :
      ?size:int -> ?shift:int -> scalar array -> Super_PP.Answers_commitment.t;
  }

  let hash_pi (pp : prover_public_parameters) ic_info inputs =
    let extract name (secrets : circuit_prover_input list) =
      let c = SMap.find name pp.circuits_map in
      let ic_size = List.fold_left ( + ) 0 c.input_com_sizes in
      List.map
        (fun s -> Array.sub s.witness ic_size c.public_input_size)
        secrets
    in
    let pi =
      SMap.mapi extract inputs |> SMap.values |> List.concat |> Array.concat
    in
    ic_info.func ~size:ic_info.nb_max_pi pi

  let prove_list (pp : prover_public_parameters) ~input_commit_info ~inputs =
    assert (SMap.cardinal pp.circuits_map = 1);
    assert (SMap.cardinal inputs = 1);
    (* Rename inputs as circuit *)
    let inputs =
      SMap.map (fun _ -> snd @@ SMap.choose inputs) pp.circuits_map
    in
    (* TODO: can we commit only to the hidden pi?*)
    let cm_pi = hash_pi pp input_commit_info inputs in
    (* add the PI in the transcript *)
    let transcript =
      PP.Answers_commitment.(Transcript.expand public_t cm_pi.public)
        pp.transcript
    in
    let input_commit =
      input_commit_info.func ~shift:input_commit_info.nb_max_pi
        ~size:input_commit_info.nb_max_answers
    in
    let ( ( pp_proof,
            Super_PP.{ answers; batch; alpha; x; r; cm_answers; t_answers } ),
          (perm_and_plook, wires_cm, rd) ) =
      Prover.prove_parameters
        ~pp_prove:(Super_PP.prove_super_aggregation ~input_commit)
        ((pp.common_pp, pp.circuits_map), transcript)
        ~inputs_map:inputs
    in
    ( { perm_and_plook; wires_cm; pp_proof },
      {
        answers;
        batch;
        alpha;
        beta = rd.beta_perm;
        gamma = rd.gamma_perm;
        delta = rd.delta;
        x;
        r;
        cm_answers;
        cm_pi;
        t_answers;
      } )

  let verify_list pp (proof, s_list, cm_answers, cm_pi) =
    assert (SMap.cardinal pp.circuits_map = 1);
    (* add the PI in the transcript *)
    let transcript =
      (Transcript.expand PP.Answers_commitment.public_t cm_pi) pp.transcript
    in
    let transcript, _, rd, commitments, eval_points =
      (* Note that we don’t care about inputs here, because verify_parameters
         only cares about input_coms & identities that we don’t have here *)
      Verifier.verify_parameters
        ((pp.common_pp, pp.circuits_map), transcript)
        SMap.empty proof
    in
    let (kzg_verif, Super_PP.{ alpha; x; r }), _transcript =
      Super_PP.verify_super_aggregation pp.common_pp.pp_public_parameters
        transcript ~n:pp.common_pp.n ~generator:pp.common_pp.generator
        ~commitments ~eval_points ~s_list ~cm_answers proof.pp_proof
    in
    ( kzg_verif,
      {
        alpha;
        beta = rd.beta_perm;
        gamma = rd.gamma_perm;
        delta = rd.delta;
        x;
        r;
      } )

  let get_gen_n_nbt_prover (prover_public_params : prover_public_parameters) =
    ( Domain.get prover_public_params.common_pp.domain 1,
      prover_public_params.common_pp.n,
      prover_public_params.common_pp.nb_of_t_chunks )

  let get_gen_n_verifier (verifier_public_params : verifier_public_parameters) =
    ( verifier_public_params.common_pp.generator,
      verifier_public_params.common_pp.n )
end

module type S = sig
  module PP : Polynomial_protocol.S
  include Plonk.Main_protocol.S
  module Gates : Plonk.Custom_gates.S
  module Perm : Plonk.Permutation_gate.S with module PP := PP

  val get_gen_n_nbt_prover : prover_public_parameters -> scalar * int * int
  (** Returns (g, n, nb_t), where n is the size of the circuit padded to the
      next power of two, g is a primitive n-th root of unity, & nb_t is the
      number of T polynomials in the answers
   *)

  val get_gen_n_verifier : verifier_public_parameters -> scalar * int
  (** Returns (g, n), where n is the size of the circuit padded to the next
      power of two & g is a primitive n-th root of unity
   *)

  type prover_aux = {
    answers : scalar SMap.t SMap.t list;
    batch : scalar SMap.t list;
    alpha : scalar;
    beta : scalar;
    gamma : scalar;
    delta : scalar;
    x : scalar;
    r : scalar;
    cm_answers : PP.Answers_commitment.t;
    cm_pi : PP.Answers_commitment.t;
    t_answers : scalar list;
  }
  (** Auxiliary information needed by the prover for the meta-verification in
      aPlonK *)

  type verifier_aux = {
    alpha : scalar;
    beta : scalar;
    gamma : scalar;
    delta : scalar;
    x : scalar;
    r : scalar;
  }
  (** Auxiliary information needed by the verifier for the meta-verification in
      aPlonK *)

  type input_commit_info = {
    nb_max_answers : int;
    nb_max_pi : int;
    func : ?size:int -> ?shift:int -> scalar array -> PP.Answers_commitment.t;
  }

  val prove_list :
    prover_public_parameters ->
    input_commit_info:input_commit_info ->
    inputs:prover_inputs ->
    proof * prover_aux

  val verify_list :
    verifier_public_parameters ->
    proof
    * scalar SMap.t list
    * PP.Answers_commitment.public
    * PP.Answers_commitment.public ->
    bool * verifier_aux
end

module Make : functor (PP : Polynomial_protocol.S) ->
  S with module PP = PP and type public_inputs = Scalar.t array list =
  Make_impl

include Make (Polynomial_protocol)
