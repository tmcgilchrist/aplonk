(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Plonk.Bls
open Plonk.Utils
open Plonk.Identities
module SMap = Plonk.SMap

module Make_impl
    (Super_PC : Polynomial_commitment.S)
    (Answers_commitment : Plonk.Input_commitment.S) =
struct
  include Plonk.Polynomial_protocol.Make_impl (Super_PC)
  module PC = Super_PC
  module Answers_commitment = Answers_commitment

  type prover_aux = {
    answers : Scalar.t SMap.t SMap.t list;
    batch : Scalar.t SMap.t list;
    alpha : Scalar.t;
    x : Scalar.t;
    r : Scalar.t;
    cm_answers : Answers_commitment.t;
    t_answers : Scalar.t list;
  }

  type verifier_aux = { alpha : Scalar.t; x : Scalar.t; r : Scalar.t }

  let prove_super_aggregation pc_public_parameters transcript ~input_commit ~n
      ~generator ~secrets ~eval_points ~evaluations ~identities ~nb_of_t_chunks
      =
    let ( (alpha, x, answer_list, cm_t),
          polys_list,
          prover_aux_list,
          query_list,
          transcript ) =
      prove_aux pc_public_parameters transcript n generator secrets eval_points
        evaluations identities nb_of_t_chunks
    in
    (* We treat t answers as Public inputs instead of through the
       input_commitment because they are not circuit-specific *)
    let t_answers, circuit_answers =
      (List.hd answer_list, List.tl answer_list)
    in
    let t_answers = t_answers |> SMap.values |> List.concat_map SMap.values in
    let cm_answers =
      circuit_answers
      |> List.concat_map SMap.values
      |> List.concat_map SMap.values
      |> Array.of_list |> input_commit
    in
    let transcript =
      Transcript.expand Answers_commitment.public_t
        Answers_commitment.(cm_answers.public)
        transcript
    in
    let (pc_proof, Super_PC.{ r; s_list }), transcript =
      Super_PC.prove_super_aggregation pc_public_parameters transcript
        polys_list prover_aux_list query_list answer_list
    in
    ( ( { cm_t; pc_proof; pc_answers = [] },
        {
          answers = answer_list;
          cm_answers;
          batch = s_list;
          alpha;
          x;
          r;
          t_answers;
        } ),
      transcript )

  let verify_super_aggregation pc_public_parameters transcript ~n:_ ~generator
      ~commitments ~eval_points ~s_list ~cm_answers proof =
    let alpha, x, transcript, cmts, query_list =
      verify_aux transcript generator commitments eval_points proof
    in
    let transcript =
      Transcript.expand Answers_commitment.public_t cm_answers transcript
    in
    (* Step 2a: KZG.verify proofs for witness combinations *)
    let pc_verif, r, transcript =
      Super_PC.verify_super_aggregation pc_public_parameters transcript cmts
        query_list s_list proof.pc_proof
    in
    ((pc_verif, { alpha; x; r }), transcript)
end

module type S = sig
  module PC : Polynomial_commitment.S
  module Answers_commitment : Plonk.Input_commitment.S
  include Plonk.Polynomial_protocol.S with module PC := PC

  type prover_aux = {
    answers : Scalar.t SMap.t SMap.t list;
    batch : Scalar.t SMap.t list;
    alpha : Scalar.t;
    x : Scalar.t;
    r : Scalar.t;
    cm_answers : Answers_commitment.t;
    t_answers : Scalar.t list;
  }
  (** Auxiliary information needed by the prover for the meta-verification in
      aPlonK *)

  type verifier_aux = { alpha : Scalar.t; x : Scalar.t; r : Scalar.t }
  (** Auxiliary information needed by the verifier for the meta-verification in
      aPlonK *)

  val prove_super_aggregation :
    prover_public_parameters ->
    transcript ->
    input_commit:(Scalar.t array -> Answers_commitment.t) ->
    n:int ->
    generator:Scalar.t ->
    secrets:(Poly.t SMap.t * PC.Commitment.prover_aux) list ->
    eval_points:eval_point list list ->
    evaluations:Evaluations.t SMap.t ->
    identities:prover_identities ->
    nb_of_t_chunks:int ->
    (proof * prover_aux) * transcript

  val verify_super_aggregation :
    verifier_public_parameters ->
    transcript ->
    n:int ->
    generator:Scalar.t ->
    commitments:PC.Commitment.t list ->
    eval_points:eval_point list list ->
    s_list:Scalar.t SMap.t list ->
    cm_answers:Answers_commitment.public ->
    proof ->
    (bool * verifier_aux) * PC.transcript
end

module Make_aggregation : functor
  (Super_PC : Polynomial_commitment.S)
  (Answers_commitment : Plonk.Input_commitment.S)
  -> S with module Answers_commitment = Answers_commitment =
  Make_impl

module KZG_Answers_commitment =
  Plonk.Input_commitment.Make (Plonk.Polynomial_commitment)

include Make_aggregation (Polynomial_commitment) (KZG_Answers_commitment)
