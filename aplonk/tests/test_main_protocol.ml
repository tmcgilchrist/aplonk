(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module External = struct
  module Cases = Plonk_test.Cases

  let no_pi_cases =
    Cases.Unit_tests_for_each_selector.list @ [ Cases.General.no_public_inputs ]

  let one_pi_cases =
    Cases.General_circuits.list @ Cases.General.list_one_public_input

  let pi_rollup_cases = Cases.(aggregate_cases Big_circuit.list)

  let upper_bound_no_pi ~zero_knowledge () =
    let module Main =
      Aplonk.Main_protocol.Make (Aplonk.Pi_parameters.No_public_input) in
    let module H = Plonk_test.Helpers.Make (Main) in
    let open Plonk_test.Cases in
    let qc = Unit_tests_for_each_selector.qc in
    let circuit_c =
      Plonk.Circuit.make ~wires:qc.circuit.wires ~gates:qc.circuit.gates
        ~public_input_size:qc.circuit.public_input_size ()
    in
    let circuits = Plonk.SMap.of_list [ (qc.name, (circuit_c, 4)) ] in
    let inputs = Plonk.SMap.singleton qc.name [ qc.witness ] in
    H.test_circuits ~zero_knowledge circuits inputs

  let upper_bound_pi_rollup ~zero_knowledge () =
    let module Main =
      Aplonk.Main_protocol.Make (Aplonk.Pi_parameters.Rollup_example) in
    let module H = Plonk_test.Helpers.Make (Main) in
    let open Plonk_test.Cases in
    let nb_proofs = 3 in
    let nb_proofs_added = 1 in
    let _name, circuits_map, inputs, _outcome =
      Big_circuit.make ~nb_proofs ~public_input_size:2 ~k:3 |> aggregate_cases
    in
    let circuits =
      Plonk.SMap.map (fun (c, n) -> (c, n + nb_proofs_added)) circuits_map
    in
    H.test_circuits ~zero_knowledge circuits inputs

  let tests_quick pc_name =
    let prefix s (n, f) = (s ^ "." ^ n, f) in
    let no_pi_tests =
      let module Main =
        Aplonk.Main_protocol.Make (Aplonk.Pi_parameters.No_public_input) in
      let module H = Plonk_test.Helpers.Make (Main) in
      List.map
        (fun case -> (Cases.(case.name), H.run_test_case case ~verbose:false))
        no_pi_cases
    in
    let one_pi_tests =
      let module Main =
        Aplonk.Main_protocol.Make (Aplonk.Pi_parameters.One_public_input) in
      let module H = Plonk_test.Helpers.Make (Main) in
      List.map
        (fun case -> (Cases.(case.name), H.run_test_case case ~verbose:false))
        one_pi_cases
    in
    let pi_rollup_case =
      let module Main =
        Aplonk.Main_protocol.Make (Aplonk.Pi_parameters.Rollup_example) in
      let module H = Plonk_test.Helpers.Make (Main) in
      let name, circuits_map, inputs_map, outcome =
        Cases.(aggregate_cases Big_circuit.list)
      in
      ( name,
        fun ~zero_knowledge () ->
          H.test_circuits ~name ~zero_knowledge circuits_map inputs_map ~outcome
      )
    in
    no_pi_tests @ one_pi_tests @ [ pi_rollup_case ]
    @ [
        ("nb_proofs no pi", upper_bound_no_pi);
        ("nb_proofs pi_rollup", upper_bound_pi_rollup);
      ]
    |> List.map (prefix pc_name)
end

let tests =
  List.map
    (fun (n, f) -> Alcotest.test_case n `Quick (f ~zero_knowledge:false))
    (External.tests_quick "aplonk")
