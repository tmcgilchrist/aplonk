(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* This file implements the aPlonK protocol, which consists in putting some original PlonK’s verifier work in a PlonK circuit (called here "aggregation circuit") that will be proved & verified instead of having to do the whole computation. This method allows for smaller proof size (most evaluations are not given in the proof anymore) & logarithmic verification time in the number of proofs (verifier’s linear operation are done in the aggregation circuit). *)

open Plonk.Bls
module SMap = Plonk.SMap

(* Main_KZG is used on the verification circuit *)
module Main_KZG = Plonk.Main_protocol

module Super_PP =
  Aggregation.Polynomial_protocol.Make_aggregation
    (Aggregation.Polynomial_commitment)
    (Main_KZG.Input_commitment)

(* Main_Pack is used in Main_proto on the circuit to prove;
   it is build with the super aggregation *)
module Main_Pack = Aggregation.Main_protocol.Make (Super_PP)
module Aggreg_circuit = Circuit.V (Main_Pack)

module Make (PI : Pi_parameters.S) :
  Plonk.Main_protocol.S
    with module Input_commitment = Main_Pack.Input_commitment
     and type circuit_prover_input = Main_Pack.circuit_prover_input
     and type public_inputs = Scalar.t list = struct
  exception Rest_not_null of string
  exception Entry_not_in_table of string

  module Input_commitment = Main_Pack.Input_commitment

  type scalar = Scalar.t [@@deriving repr]
  type circuit_map = Main_Pack.circuit_map

  let public_input_size ~nb_t gates =
    Aggreg_circuit.meta_public_input_size ~nb_t gates + PI.nb_outer

  type prover_public_parameters = {
    main_circuit : Main_Pack.prover_public_parameters;
    aggreg_circuit : Main_KZG.prover_public_parameters;
    (* The solver is needed to create the witness in the prover *)
    aggreg_solver : Plompiler.Solver.t;
    public_input_size : int;
    input_com_sizes : int list;
    nb_proofs : int;
  }
  [@@deriving repr]

  type verifier_public_parameters = {
    main_circuit : Main_Pack.verifier_public_parameters;
    aggreg_circuit : Main_KZG.verifier_public_parameters;
    public_input_size : int;
  }
  [@@deriving repr]

  type proof = {
    main : Main_Pack.proof;
    aggreg : Main_KZG.proof;
    batch : Main_KZG.scalar SMap.t list;
    cm_answers : Super_PP.Answers_commitment.public;
    cm_pi : Super_PP.Answers_commitment.public;
    t_answers : Main_KZG.scalar list;
  }
  [@@deriving repr]

  type circuit_prover_input = Main_Pack.circuit_prover_input = {
    witness : scalar array;
    input_commitments : Main_Pack.Input_commitment.t list;
  }

  type prover_inputs = circuit_prover_input list SMap.t
  type public_inputs = scalar list

  type verifier_inputs =
    (public_inputs * Input_commitment.public list list) SMap.t

  let to_verifier_inputs (pp : prover_public_parameters) inputs =
    let vi = Main_Pack.to_verifier_inputs pp.main_circuit inputs in
    SMap.map
      (fun (pi, ic) ->
        let pi = PI.outer_of_inner (List.map Array.to_list pi) in
        assert (List.for_all (fun i -> i = []) ic);
        (pi, ic))
      vi

  module Internal_for_tests = struct
    let mutate_vi verifier_inputs =
      let key, (public_inputs, input_commitments_list) =
        SMap.choose verifier_inputs
      in
      match public_inputs with
      | [] -> None
      | input ->
          let input = Array.of_list input in
          let i = Random.int (Array.length input) in
          input.(i) <- Scalar.random ();
          Some
            (SMap.add key
               (Array.to_list input, input_commitments_list)
               verifier_inputs)
  end

  let update_prover_public_parameters bytes (pp : prover_public_parameters) =
    {
      pp with
      main_circuit =
        Main_Pack.update_prover_public_parameters bytes pp.main_circuit;
      aggreg_circuit =
        Main_KZG.update_prover_public_parameters bytes pp.aggreg_circuit;
    }

  let update_verifier_public_parameters bytes (pp : verifier_public_parameters)
      =
    {
      pp with
      main_circuit =
        Main_Pack.update_verifier_public_parameters bytes pp.main_circuit;
      aggreg_circuit =
        Main_KZG.update_verifier_public_parameters bytes pp.aggreg_circuit;
    }

  (* used for debug with sat *)
  let cs_global = ref []

  (* ////////////////////////////////////////////////////// *)

  let input_commit ?size ?shift (pp : prover_public_parameters) secret =
    ignore (size, shift, pp, secret);
    failwith "[input_commit] in aPlonK is not supported yet"

  let setup ~zero_knowledge circuits_map ~srs =
    (* TODO: generalize aPlonK to handle different circuits *)
    assert (SMap.cardinal circuits_map = 1);
    let circuit, nb_proofs = snd @@ SMap.choose circuits_map in
    let prover_pp, verifier_pp =
      Main_Pack.setup ~zero_knowledge circuits_map ~srs
    in
    let cs =
      Aggreg_circuit.get_cs_verification prover_pp circuit nb_proofs
        PI.(nb_outer, nb_inner)
        PI.check
    in
    (* cs_global is used for sat *)
    cs_global := cs.cs;
    (* Plompiler.Utils.dump_label_traces
       ("../../../../flamegraph/flamegraph" ^ "_" ^ Int.to_string nb_proofs)
       cs.cs; *)
    let _, _, nb_t = Main_Pack.get_gen_n_nbt_prover prover_pp in
    let public_input_size = public_input_size ~nb_t circuit.gates in
    let input_com_sizes = cs.input_com_sizes in
    let circuit_aggreg =
      Plonk.Circuit.to_plonk ~public_input_size ~input_com_sizes cs.cs
    in
    let agg_circuit_map = SMap.singleton "" (circuit_aggreg, 1) in
    let aggreg_solver = cs.solver in

    let prover_pp_aggreg, verifier_pp_aggreg =
      Main_KZG.setup ~zero_knowledge agg_circuit_map ~srs
    in
    ( ({
         main_circuit = prover_pp;
         aggreg_circuit = prover_pp_aggreg;
         aggreg_solver;
         public_input_size;
         input_com_sizes;
         nb_proofs;
       }
        : prover_public_parameters),
      {
        main_circuit = verifier_pp;
        aggreg_circuit = verifier_pp_aggreg;
        public_input_size;
      } )

  let prove (pp : prover_public_parameters) ~(inputs : prover_inputs) =
    assert (SMap.cardinal inputs = 1);
    let input_commit_info =
      Main_Pack.
        {
          nb_max_pi = List.hd pp.input_com_sizes;
          nb_max_answers = List.nth pp.input_com_sizes 1;
          func =
            (fun ?size ?shift s ->
              Main_KZG.input_commit ?size ?shift pp.aggreg_circuit s);
        }
    in
    let main_proof, prover_aux =
      try Main_Pack.prove_list pp.main_circuit ~input_commit_info ~inputs
      with Main_Pack.Rest_not_null _ ->
        raise
          (Rest_not_null
             "Main_Pack.prove could not create a proof for the base circuit.")
    in
    let inner_pi =
      Main_Pack.to_verifier_inputs pp.main_circuit inputs
      |> SMap.values |> List.map fst |> List.concat |> List.map Array.to_list
    in
    let switches, compressed_switches =
      Aggreg_circuit.compute_switches pp.nb_proofs inputs
    in
    let aggreg_proof =
      let trace =
        let outer_pi = PI.outer_of_inner inner_pi in
        Aggreg_circuit.get_witness pp.nb_proofs prover_aux pp.public_input_size
          pp.aggreg_solver (inner_pi, outer_pi) switches compressed_switches
      in
      let secret =
        Main_KZG.
          {
            witness = trace;
            input_commitments = [ prover_aux.cm_pi; prover_aux.cm_answers ];
          }
      in
      (* assert (Plonk.Circuit.sat !cs_global [] trace); *)
      let inputs = SMap.singleton "" [ secret ] in
      let pp_aggreg_circuit =
        Main_KZG.update_prover_public_parameters
          (Data_encoding.Binary.to_bytes_exn Main_Pack.proof_encoding main_proof)
          pp.aggreg_circuit
      in
      try Main_KZG.prove pp_aggreg_circuit ~inputs
      with Main_KZG.Rest_not_null _ ->
        raise
          (Rest_not_null
             "Main_Kzg.prove could not create a proof for the verification \
              circuit.")
    in
    {
      main = main_proof;
      aggreg = aggreg_proof;
      batch = prover_aux.batch;
      cm_answers = prover_aux.cm_answers.public;
      cm_pi = prover_aux.cm_pi.public;
      t_answers = prover_aux.t_answers;
    }

  let verify pp ~(inputs : verifier_inputs) proof =
    assert (SMap.cardinal inputs = 1);
    let pi, input_commitments_list = snd @@ SMap.choose inputs in
    let nb_proofs = List.length input_commitments_list in
    let main_verif, verifier_aux =
      Main_Pack.verify_list pp.main_circuit
        (proof.main, proof.batch, proof.cm_answers, proof.cm_pi)
    in
    if List.exists (fun l -> l <> []) input_commitments_list then
      raise
      @@ Invalid_argument
           "input commitments in the base circuit of\n\
           \           aPlonK are not yet supported";
    let public_inputs =
      let v = verifier_aux in
      Aggreg_circuit.aggreg_public_inputs pp.public_input_size
        (v.alpha, v.beta, v.gamma, v.delta, v.x, v.r)
        proof.batch proof.t_answers (Scalar.of_int nb_proofs) pi
    in
    let pp_aggreg_circuit =
      Main_KZG.update_verifier_public_parameters
        (Data_encoding.Binary.to_bytes_exn Main_Pack.proof_encoding proof.main)
        pp.aggreg_circuit
    in
    let aggreg_verif =
      let inputs =
        SMap.singleton ""
          ([ public_inputs ], [ [ proof.cm_pi; proof.cm_answers ] ])
      in
      Main_KZG.verify pp_aggreg_circuit ~inputs proof.aggreg
    in
    main_verif && aggreg_verif

  (* Encodings *)

  let scalar_encoding = Main_Pack.scalar_encoding

  let data_encoding_of_repr repr =
    Data_encoding.conv
      (Plompiler.Utils.to_bytes repr)
      (Plompiler.Utils.of_bytes repr)
      Data_encoding.bytes

  let proof_encoding = data_encoding_of_repr proof_t

  let verifier_public_parameters_encoding =
    data_encoding_of_repr verifier_public_parameters_t
end
